'''
Author: Shreejal Trivedi

Date: 16th July 2020

Description: Inference code of the YOLOv3 + ROIClass Branch. This file contains the inference class definition named YOLOv3ROIClass.
             
'''

from __future__ import print_function
from __future__ import division
import torch
from torchvision import transforms
import os
import numpy as np
from utils import get_all_boxes, nms, read_data_cfg, logging
from darknet_v2 import Darknet
from image import correct_yolo_boxes, correct_proposal_boxes, letterbox_image, draw_bbox, convert_from_YOLO
from PIL import Image
import copy
from bgsrunner import BGSRunner
import cv2

class YOLOv3ROIClassTester():

    def __init__(self, datafile, cfgfile, weightfile, weightfile_roiclass=None, use_cuda=None, 
                    conf_thresh=0.05, nms_thresh=0.45, min_prop_thresh=0.05, max_prop_thresh=0.5, seed=2222, USE_CLASS=False, DEBUG=False, INTEGRATE_BGS=False):

        # Class Initializations

        self.datafile = datafile
        self.cfgfile = cfgfile
        self.weightfile = weightfile
        self.weightfile_roiclass = weightfile_roiclass
        self.use_cuda = torch.cuda.is_available() and (True if use_cuda is None else use_cuda)
        self.device = torch.device('cuda' if self.use_cuda else 'cpu')
        self.seed = seed
        self.transform = transforms.Compose([transforms.ToTensor()])
        self.use_class = USE_CLASS
        self.debug = DEBUG
        self.INTEGRATE_BGS = INTEGRATE_BGS

        # Inference value initializations

        self.conf_thresh = conf_thresh
        self.nms_thresh = nms_thresh
        self.min_prop_thresh = min_prop_thresh
        self.max_prop_thresh = max_prop_thresh

        # Intialize the model

        self.init_detector()

        # Initialize BGS model

        if self.INTEGRATE_BGS:
            self.runner = BGSRunner(width=self.model.width, height=self.model.height, bgs_algorithm='GSOC')
            print('BGS INTEGRATION is ON...Please use video frames for a good BGS motion update. Random frames not recommended..!')

        # Make some folders for storing results and text outputs

        if not os.path.exists('detection_results'):
            os.mkdir('detection_results')
        if not os.path.exists('detection_texts'):
            os.mkdir('detection_texts')

    
    def init_detector(self):

        # Parse Datafile

        data_options = read_data_cfg(self.datafile)
        gpus = data_options['gpus']
        ngpus = len(gpus.split(','))

        # Initialize Detector

        self.model = Darknet(self.cfgfile, self.use_cuda)
        self.model.load_weights(self.weightfile)
        if self.weightfile_roiclass is not None and len(self.model.roi_layers) > 0 and self.use_class:
            self.model.load_weights_roi(self.weightfile_roiclass)

        # GPU Configurations if any

        torch.manual_seed(self.seed)

        if self.use_cuda:
            os.environ['CUDA_VISIBLE_DEVICES'] = gpus
            torch.cuda.manual_seed(self.seed)

        if self.use_cuda:
            if ngpus > 1:
                self.model = torch.nn.DataParallel(self.model)
                self.model = self.model.module
        
        # Transfer the model onto GPU/CPU device

        self.model = self.model.to(self.device)
        self.model.eval()


    def preprocess_image(self, frame):

        # Convert CV2 to PIL Image 

        frame = Image.fromarray(frame)
        
        (frame_width, frame_height) = frame.size

        frame = letterbox_image(frame, self.model.width, self.model.height)

        # Apply transforms

        frame = self.transform(frame)
        frame = frame.to(self.device)
        frame = frame.unsqueeze(0)

        return frame, frame_width, frame_height
    

    def convert_proposals(self, proposals, idxs):

        prop = proposals[idxs]
        prop = np.roll(prop, 1, axis=1)
        prop[:, 0] = 0
        prop = prop[:, :5]
        prop = convert_from_YOLO(prop, self.model.width, self.model.height)

        prop = torch.from_numpy(prop)

        return prop
    
    def convert_from_proposals(self, boxes, im_w, im_h, net_w, net_h):

        if boxes.shape[0] > 0:

            if boxes.shape[1] == 5:
                c, x1, y1, x2, y2 = np.split(boxes, 5, axis=1)

            else:
                x1, y1, x2, y2 = np.split(boxes, 4, axis=1)
                c = np.zeros(shape=(boxes.shape[0], 1))

            xc = ((x1 + x2) / 2.) / net_w
            yc = ((y1 + y2) / 2.) / net_h
            w = (x2 - x1) / net_w
            h = (y2 - y1) / net_h

            bboxes = np.hstack((xc, yc, w, h))

            im_w, im_h = float(im_w), float(im_h)

            net_w, net_h = float(net_w), float(net_h)

            if net_w / im_w < net_h / im_h:
                new_w = net_w
                new_h = (im_h * net_w) / im_w
            else:
                new_w = (im_w * net_h) / im_h
                new_h = net_h

            xo, xs = (net_w - new_w) / (2 * net_w), new_w / net_w
            yo, ys = (net_h - new_h) / (2 * net_h), new_h / net_h

            bboxes[:, 0] = (bboxes[:, 0] - xo) / xs
            bboxes[:, 1] = (bboxes[:, 1] - yo) / ys
            bboxes[:, 2] = bboxes[:, 2] / xs
            bboxes[:, 3] = bboxes[:, 3] / ys

            bboxes = convert_from_YOLO(np.hstack((c, bboxes)), im_w, im_h)

            return bboxes[:, 1:]
        else:
            return np.array([])

    def convert_boxes(self, boxes, frame_width, frame_height):

        bboxes = np.roll(boxes, 1, axis=1)
        bboxes[:, 0] = 0
        bboxes = bboxes[:, :5]
        bboxes = convert_from_YOLO(bboxes, frame_width, frame_height)
        return bboxes[:, 1:]

    def convert_PIL_to_CV(self, img):

        opencv_img = np.array(img)[:, :, ::-1]
        return opencv_img

    def dump_detection_results(self, boxes, frame, imgname, prediction_list=None):
        
        for i, bb in enumerate(boxes):
            
            color = 'green'
            
            if prediction_list is not None:
                if i in prediction_list:
                    color = 'blue'
                elif bb[4] < self.max_prop_thresh: 
                    color = 'red'

            draw_bbox(frame, self.convert_boxes(bb.reshape(1, -1), frame.size[0], frame.size[1]), color=color)

        frame.save(os.path.join('detection_results', str(imgname) + '.jpg'))
    
    def dump_detection_texts(self, boxes, frame_width, frame_height, imgname, prediction_list=None):
        
        if boxes.shape[0] > 0:
            xmin, ymin, xmax, ymax = (boxes[:, 0] - boxes[:, 2] / 2.0) * frame_width, (boxes[:, 1] - boxes[:, 3] / 2.0) * frame_height, (boxes[:, 0] + boxes[:, 2] / 2.0) * frame_width, (boxes[:, 1] + boxes[:, 3] / 2.0) * frame_height
            probs = boxes[:, 4] * boxes[:, 5]
            class_ids = boxes[:, 6]        
            detection_box = np.transpose(np.vstack((class_ids, probs, xmin, ymin, xmax, ymax)))
            
            if prediction_list is not None:
                detector_idxs = np.where(detection_box[:, 1] > self.max_prop_thresh)[0]
                if prediction_list.shape[0] > 0:
                    detection_box[prediction_list][:, 1] = 0.5
                    detector_idxs = np.array(detector_idxs.tolist() + prediction_list.tolist())    
                
                detection_box = detection_box[detector_idxs]
        else:
            detection_box = np.array([])

        np.savetxt(os.path.join('detection_texts', str(imgname).replace('.jpg', '.txt')), detection_box)
    

    def run_detector(self, frame, imgname):

        ''' 
        Main Function: Takes Frame and image name as an input and save the results of the Dectector/Detector + Classifier output in the folder
                        'detection_results' and 'detection_texts'
                    
        '''

        frame_orig = copy.deepcopy(frame)
        frame_orig = Image.fromarray(frame_orig)
        frame, frame_width, frame_height = self.preprocess_image(frame)
        shape = (self.model.width, self.model.height)

        # Forward Pass: If USE_CLASS is true then only do forward pass in classifer branch

        if self.use_class and len(self.model.roi_layers) > 0:
            output, output_fm = self.model(frame)
        else:
            output = self.model(frame)

        if self.INTEGRATE_BGS: # Apply BGS algorithm to the present frame
            img_bgs = self.convert_PIL_to_CV(transforms.ToPILImage()(frame[0].cpu()))
            self.runner.apply(img_bgs)

        all_boxes = get_all_boxes(output, shape, self.conf_thresh, self.model.num_classes, use_cuda=self.use_cuda, use_class=self.use_class)

        for k in range(len(all_boxes)):

            boxes = all_boxes[k]
            correct_yolo_boxes(boxes, frame_width, frame_height, self.model.width, self.model.height)
            boxes = np.array(nms(boxes, self.nms_thresh))
            frame_width, frame_height = float(frame_width), float(frame_height)

            # This is the classifier branch.
             
            if self.use_class and len(self.model.roi_layers) > 0:
                
                prediction_list = list()
                if boxes.shape[0] > 0:

                    # Extract proposal boxes and double check if shape of original boxes and proposals are same or not
    
                    proposals = all_boxes[k]
                    correct_proposal_boxes(proposals, frame_width, frame_height, self.model.width, self.model.height)
                    proposals = np.array(nms(proposals, self.nms_thresh))
                    assert(boxes.shape[0] == proposals.shape[0])

                    proposal_idxs = np.where((proposals[:, 4] >= self.min_prop_thresh) & (proposals[:, 4] < self.max_prop_thresh))[0]
                    proposals = self.convert_proposals(proposals, proposal_idxs)

                    # Update Detection Mask by passing proposals to the runner

                    if self.INTEGRATE_BGS: 

                        bgs_contours = self.runner.getCountours()
                        self.runner.updateDetMask(proposals.cpu().numpy())
                        boxes_final_bgs = self.runner.checkUnwantedBoxes(proposals[:, 1:].cpu().numpy(), bgs_contours)
                    
                    # Forward pass in roiclass branch

                    for j, l in enumerate(self.model.roi_layers):

                        proposals_filters, idx_filters = l.candidate_proposals_validation(proposals)

                        if proposals_filters.shape[0] > 0:

                            proposals_filters_total = proposals_filters
                            if self.INTEGRATE_BGS:
                                boxes_final_bgs = torch.Tensor(boxes_final_bgs).long().to(self.device) # Convert the boxes to Tensor
                                proposals_filters_total = torch.cat([proposals_filters, boxes_final_bgs], dim=0) if boxes_final_bgs.size(0) > 0 else proposals_filters
                            
                            out = l(output_fm[j]['x'], proposals_filters_total)
                            pred_total = np.argmax(out.data.cpu().numpy(), axis=1)
                            pred = pred_total[:proposals_filters.size(0)]

                            if self.INTEGRATE_BGS:

                                pred_bgs = pred_total[-boxes_final_bgs.size(0):] if boxes_final_bgs.size(0) > 0 else []
                                boxes_final_bgs_classified = boxes_final_bgs[np.where(pred_bgs == 1)].cpu().numpy()
                                boxes_final_bgs_non_classified = boxes_final_bgs[np.where(pred_bgs == 0)].cpu().numpy()
                                
                                self.runner.updateDetMask(boxes_final_bgs_classified, flag=False)
                                self.runner.update(img_bgs)
                                
                                boxes_final_bgs_classified = self.convert_from_proposals(boxes_final_bgs_classified, frame_orig.size[0], frame_orig.size[1], self.model.width, self.model.height)
                                boxes_final_bgs_non_classified = self.convert_from_proposals(boxes_final_bgs_non_classified, frame_orig.size[0], frame_orig.size[1], self.model.width, self.model.height)
       
                            # Save the proposals indices classifed as a person in this branch for future refinements
                            prediction_list.append(list(proposal_idxs[idx_filters[np.where(pred == 1)]]))

                    prediction_list = np.array(sum(prediction_list, []))

        if self.debug:
            if self.use_class and len(self.model.roi_layers) > 0:

                if self.INTEGRATE_BGS: # Draw BGS Boxes
                    draw_bbox(frame_orig, boxes_final_bgs_classified, color='cyan')
                    draw_bbox(frame_orig, boxes_final_bgs_non_classified, color='orange')

                self.dump_detection_results(boxes, frame_orig, imgname, prediction_list=prediction_list)
            else:
                self.dump_detection_results(boxes, frame_orig, imgname)

        if self.use_class and len(self.model.roi_layers) > 0:
            self.dump_detection_texts(boxes, frame_width, frame_height, imgname, prediction_list)
        else:
            self.dump_detection_texts(boxes, frame_width, frame_height, imgname)