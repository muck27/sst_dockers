from __future__ import division
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from cfg import *
import numpy as np
from yolo_layer import YoloLayer
from torchvision.ops import RoIPool
from torchvision import transforms
import cv2
import time


class MaxPoolStride1(nn.Module):
    def __init__(self):
        super(MaxPoolStride1, self).__init__()

    def forward(self, x):
        x = F.max_pool2d(F.pad(x, (0,1,0,1), mode='replicate'), 2, stride=1)
        return x

class ROIClass(nn.Module):
    def __init__(self, scaled_window, spatial_scale, out_filters, in_filters, class_size="all", total_proposals=2, num_classes=2, use_cuda=None):
        super(ROIClass, self).__init__()
        use_cuda = torch.cuda.is_available() and (True if use_cuda is None else use_cuda)
        self.device = torch.device("cuda" if use_cuda else "cpu")

        self. scaled_window = scaled_window
        self.spatial_scale = spatial_scale
        self.total_proposals = total_proposals
        self.out_filters = out_filters
        self.in_filters = in_filters
        self.num_classes = num_classes
        self.seen = 0
        self.net_width = 0
        self.net_height = 0
        self.feature_map = None
        self.class_size = class_size
        self.area_small = 32 * 32
        self.area_medium = 96 * 96

        '''
        #ROI Pooling layer for fixating the size of the proposals: 
        # Input --> Feature Map[batch_size X in_channels X in_width X in_height], ROIS[total_proposals X 5]
        
        # Output --> [total_proposals X in_channels X scaled_window X scaled_window]
        '''

        self.roi_pooling = RoIPool(output_size=(self.scaled_window, self.scaled_window), spatial_scale=self.spatial_scale)
        
        #Convolution Layer 1
        self.conv1 = nn.Conv2d(self.in_filters, self.out_filters, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(self.out_filters)
        
        #Convolution Layer 2
        self.conv2 = nn.Conv2d(self.out_filters, self.out_filters, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(self.out_filters)
        
        #Average Pool Layer 
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        #Out Layer: BG/FG
        self.linear = nn.Linear(self.out_filters, num_classes)

    def return_fm(self, fm):
        return {'x': fm}
    
    def get_area(self, proposals):

        area = [(box[2] - box[0]) * (box[3] - box[1]) for box in proposals]
        return np.array(area)

    def candidate_proposals_validation(self, proposals):

        area_proposals = self.get_area(proposals[:,1:])

        #Classifier Size Independent Training
        if self.class_size == 'small':
           idxs = np.where(area_proposals < self.area_small)
           return proposals[idxs].to(self.device), idxs[0]

        elif self.class_size == 'medium':
           idxs = np.where((area_proposals >= self.area_small) & (area_proposals < self.area_medium))
           return proposals[idxs].to(self.device), idxs[0]
        
        elif self.class_size == 'large':
           idxs = np.where(area_proposals >= self.area_medium)
           return proposals[idxs].to(self.device), idxs[0]
        
        else:
           return proposals.to(self.device), np.arange(0, proposals.shape[0])


    def candidate_proposals(self, proposals, label_proposals):

        area_proposals = self.get_area(proposals[:, 1:])

        #Classifier Size Independent Training
        if self.class_size == 'small':
           idxs = np.where(area_proposals < self.area_small)
           return proposals[idxs].to(self.device), label_proposals[idxs].to(self.device)
        
        elif self.class_size == 'medium':
           idxs = np.where((area_proposals >= self.area_small) & (area_proposals < self.area_medium))
           return proposals[idxs].to(self.device), label_proposals[idxs].to(self.device)
        
        elif self.class_size == 'large':
           idxs = np.where(area_proposals >= self.area_medium)
           return proposals[idxs].to(self.device), label_proposals[idxs].to(self.device)
        
        else:
           return proposals.to(self.device), label_proposals.to(self.device)

    def forward(self, feature_map, proposals):

        proposals = proposals.to(self.device)
        #label_proposals = label_proposals.to(self.device).long()
        out = self.roi_pooling(feature_map, proposals.float())                
        out = self.conv1(out)
        out = self.bn1(out)
        out = F.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = F.relu(out)
        out = self.avgpool(out)
        out = torch.flatten(out, 1)
        out = self.linear(out)
        #loss = self.criterion(out, label_proposals)
        return out

class Upsample(nn.Module):
    def __init__(self, stride=2):
        super(Upsample, self).__init__()
        self.stride = stride
    def forward(self, x):
        stride = self.stride
        assert(x.data.dim() == 4)
        B = x.data.size(0)
        C = x.data.size(1)
        H = x.data.size(2)
        W = x.data.size(3)
        ws = stride
        hs = stride
        x = x.view(B, C, H, 1, W, 1).expand(B, C, H, hs, W, ws).contiguous().view(B, C, H*hs, W*ws)
        return x

class Reorg(nn.Module):
    def __init__(self, stride=2):
        super(Reorg, self).__init__()
        self.stride = stride
    def forward(self, x):
        stride = self.stride
        assert(x.data.dim() == 4)
        B = x.data.size(0)
        C = x.data.size(1)
        H = x.data.size(2)
        W = x.data.size(3)
        assert(H % stride == 0)
        assert(W % stride == 0)
        ws = stride
        hs = stride
        x = x.view(B, C, H//hs, hs, W//ws, ws).transpose(3,4).contiguous()
        x = x.view(B, C, (H//hs)*(W//ws), hs*ws).transpose(2,3).contiguous()
        x = x.view(B, C, hs*ws, H//hs, W//ws).transpose(1,2).contiguous()
        x = x.view(B, hs*ws*C, H//hs, W//ws)
        return x

class GlobalAvgPool2d(nn.Module):
    def __init__(self):
        super(GlobalAvgPool2d, self).__init__()

    def forward(self, x):
        N = x.data.size(0)
        C = x.data.size(1)
        H = x.data.size(2)
        W = x.data.size(3)
        x = F.avg_pool2d(x, (H, W))
        x = x.view(N, C)
        return x

# for route and shortcut
class EmptyModule(nn.Module):
    def __init__(self):
        super(EmptyModule, self).__init__()

    def forward(self, x):
        return x

# support route shortcut and reorg

class Darknet(nn.Module):
    def net_name(self):
        names_list = ('region', 'yolo')
        name = names_list[0]
        for m in self.models:
            if isinstance(m, YoloLayer):
                name = names_list[1]
        return name

    def getROILayers(self):
        roi_layers = []
        for m in self.models:
            if isinstance(m, ROIClass):
                roi_layers.append(m)

        return roi_layers

    def sample_proposals(self, proposals, label_proposals):

        proposals = proposals.cpu()
        label_proposals = label_proposals.cpu()

        proposals = np.delete(proposals, np.where(np.all(np.isclose(proposals, 0), axis=1)), axis=0)
        label_proposals = np.delete(label_proposals, np.where(np.all(np.isclose(label_proposals, 0), axis=1)), axis=0)[:,0].long()

        #Uncomment these three lines for random shuffle in minibatch of classifier
        shuffle_idxs = np.arange(0, proposals.shape[0])
        np.random.shuffle(shuffle_idxs)
        proposals = proposals[shuffle_idxs]
        label_proposals = label_proposals[shuffle_idxs]

        return proposals.to(self.device), label_proposals.to(self.device)

    def getLossLayers(self):
        loss_layers = []
        for m in self.models:
            if isinstance(m, RegionLayer) or isinstance(m, YoloLayer):
                loss_layers.append(m)
        return loss_layers

    def __init__(self, cfgfile, use_cuda=False):
        super(Darknet, self).__init__()
        self.use_cuda= use_cuda
        self.device = torch.device("cuda" if use_cuda else "cpu")
        self.blocks = parse_cfg(cfgfile)
        self.models = self.create_network(self.blocks) # merge conv, bn,leaky
        self.loss_layers = self.getLossLayers()
        self.roi_layers = self.getROILayers()

        if len(self.loss_layers) > 0:
            last = len(self.loss_layers)-1
            self.anchors = self.loss_layers[last].anchors
            self.num_anchors = self.loss_layers[last].num_anchors
            self.anchor_step = self.loss_layers[last].anchor_step
            self.num_classes = self.loss_layers[last].num_classes

        # default format : major=0, minor=1
        self.header = torch.IntTensor([0,1,0,0])
        self.seen = 0

    def forward(self, x):
        ind = -2
        #self.loss_layers = None
        outputs = dict()
        out_boxes = dict()
        out_fm = dict()
        outno = 0
        outroi=0
        for block in self.blocks:
            ind = ind + 1

            if block['type'] == 'net':
                continue
            elif block['type'] in ['convolutional', 'maxpool', 'reorg', 'upsample', 'avgpool', 'softmax', 'connected']:
                x = self.models[ind](x)
                outputs[ind] = x
            elif block['type'] == 'roiclass':
                fm = self.models[ind].return_fm(x)
                out_fm[outroi] = fm
                outroi +=1
                outputs[ind] = None
                
            elif block['type'] == 'route':
                layers = block['layers'].split(',')
                layers = [int(i) if int(i) > 0 else int(i)+ind for i in layers]
                if len(layers) == 1:
                    x = outputs[layers[0]]
                elif len(layers) == 2:
                    x1 = outputs[layers[0]]
                    x2 = outputs[layers[1]]
                    x = torch.cat((x1,x2),1)
                outputs[ind] = x
            elif block['type'] == 'shortcut':
                from_layer = int(block['from'])
                activation = block['activation']
                from_layer = from_layer if from_layer > 0 else from_layer + ind
                x1 = outputs[from_layer]
                x2 = outputs[ind-1]
                x  = x1 + x2
                if activation == 'leaky':
                    x = F.leaky_relu(x, 0.1, inplace=True)
                elif activation == 'relu':
                    x = F.relu(x, inplace=True)
                outputs[ind] = x
            elif block['type'] in [ 'region', 'yolo']:
                boxes = self.models[ind].get_mask_boxes(x)
                out_boxes[outno]= boxes
                outno += 1
                outputs[ind] = None
            elif block['type'] == 'cost':
                continue
            else:
                print('unknown type %s' % (block['type']))
        return x if outno == 0 else (out_boxes, out_fm)

    def print_network(self):
        print_cfg(self.blocks)

    def create_network(self, blocks):
        models = nn.ModuleList()
        prev_filters = 3
        out_filters =[]
        prev_stride = 1
        out_strides = []
        self.roi_index = []
        conv_id = 0
        ind = -2
        for block in blocks:
            ind += 1
            if block['type'] == 'net':
                prev_filters = int(block['channels'])
                self.width = int(block['width'])
                self.height = int(block['height'])
                prev_width = self.width
                prev_height = self.height

                continue
            elif block['type'] == 'convolutional':
                conv_id = conv_id + 1
                batch_normalize = int(block['batch_normalize'])
                filters = int(block['filters'])
                kernel_size = int(block['size'])
                stride = int(block['stride'])
                is_pad = int(block['pad'])
                pad = (kernel_size-1)//2 if is_pad else 0
                activation = block['activation']
                width = (prev_width + 2*pad - kernel_size)//stride + 1
                height = (prev_height + 2*pad - kernel_size)//stride + 1
                model = nn.Sequential()
                if batch_normalize:
                    model.add_module('conv{0}'.format(conv_id), nn.Conv2d(prev_filters, filters, kernel_size, stride, pad, bias=False))
                    model.add_module('bn{0}'.format(conv_id), nn.BatchNorm2d(filters))
                    #model.add_module('bn{0}'.format(conv_id), BN2d(filters))
                else:
                    model.add_module('conv{0}'.format(conv_id), nn.Conv2d(prev_filters, filters, kernel_size, stride, pad))
                if activation == 'leaky':
                    model.add_module('leaky{0}'.format(conv_id), nn.LeakyReLU(0.1, inplace=True))
                elif activation == 'relu':
                    model.add_module('relu{0}'.format(conv_id), nn.ReLU(inplace=True))
                prev_filters = filters
                out_filters.append(prev_filters)
                prev_width = width
                prev_height = height
                prev_stride = stride * prev_stride
                out_strides.append(prev_stride)                
                models.append(model)
            elif block['type'] == 'maxpool':
                pool_size = int(block['size'])
                stride = int(block['stride'])
                if stride > 1:
                    model = nn.MaxPool2d(pool_size, stride)
                else:
                    model = MaxPoolStride1()
                out_filters.append(prev_filters)
                prev_stride = stride * prev_stride
                out_strides.append(prev_stride)                
                models.append(model)
            elif block['type'] == 'avgpool':
                model = GlobalAvgPool2d()
                out_filters.append(prev_filters)
                models.append(model)
            elif block['type'] == 'roiclass':
                self.roi_index.append(ind)
                scaled_window = int(block['scaled_window'])
                filters_out = int(block['out_filters'])
                filters_in =int(block['in_filters'])
                class_size = str(block['class_size'])
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                assert(prev_width == prev_height) 
                model = ROIClass(scaled_window, float(prev_width)/self.width, filters_out, filters_in, class_size)
                models.append(model)
            elif block['type'] == 'softmax':
                model = nn.Softmax()
                out_strides.append(prev_stride)
                out_filters.append(prev_filters)
                models.append(model)
            elif block['type'] == 'cost':
                if block['_type'] == 'sse':
                    model = nn.MSELoss(size_average=True)
                elif block['_type'] == 'L1':
                    model = nn.L1Loss(size_average=True)
                elif block['_type'] == 'smooth':
                    model = nn.SmoothL1Loss(size_average=True)
                out_filters.append(1)
                out_strides.append(prev_stride)
                models.append(model)
            elif block['type'] == 'reorg':
                stride = int(block['stride'])
                prev_filters = stride * stride * prev_filters
                out_filters.append(prev_filters)
                prev_stride = prev_stride * stride
                out_strides.append(prev_stride)                
                models.append(Reorg(stride))
            elif block['type'] == 'upsample':
                stride = int(block['stride'])
                out_filters.append(prev_filters)
                prev_stride = prev_stride / stride
                out_strides.append(prev_stride)                
                #models.append(nn.Upsample(scale_factor=stride, mode='nearest'))
                models.append(Upsample(stride))
            elif block['type'] == 'route':
                layers = block['layers'].split(',')
                ind = len(models)
                layers = [int(i) if int(i) > 0 else int(i)+ind for i in layers]
                if len(layers) == 1:
                    prev_filters = out_filters[layers[0]]
                    prev_stride = out_strides[layers[0]]
                elif len(layers) == 2:
                    assert(layers[0] == ind - 1)
                    prev_filters = out_filters[layers[0]] + out_filters[layers[1]]
                    prev_stride = out_strides[layers[0]]
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                models.append(EmptyModule())

            elif block['type'] == 'shortcut':
                ind = len(models)
                prev_filters = out_filters[ind-1]
                out_filters.append(prev_filters)
                prev_stride = out_strides[ind-1]
                out_strides.append(prev_stride)
                models.append(EmptyModule())

            elif block['type'] == 'connected':
                filters = int(block['output'])
                if block['activation'] == 'linear':
                    model = nn.Linear(prev_filters, filters)
                elif block['activation'] == 'leaky':
                    model = nn.Sequential(
                               nn.Linear(prev_filters, filters),
                               nn.LeakyReLU(0.1, inplace=True))
                elif block['activation'] == 'relu':
                    model = nn.Sequential(
                               nn.Linear(prev_filters, filters),
                               nn.ReLU(inplace=True))
                prev_filters = filters
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                models.append(model)
            elif block['type'] == 'region':
                region_layer = RegionLayer(use_cuda=self.use_cuda)
                anchors = block['anchors'].split(',')
                region_layer.anchors = [float(i) for i in anchors]
                region_layer.num_classes = int(block['classes'])
                region_layer.num_anchors = int(block['num'])
                region_layer.anchor_step = len(region_layer.anchors)//region_layer.num_anchors
                region_layer.rescore = int(block['rescore'])
                region_layer.object_scale = float(block['object_scale'])
                region_layer.noobject_scale = float(block['noobject_scale'])
                region_layer.class_scale = float(block['class_scale'])
                region_layer.coord_scale = float(block['coord_scale'])
                region_layer.thresh = float(block['thresh'])
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                models.append(region_layer)
            elif block['type'] == 'yolo':
                yolo_layer = YoloLayer(use_cuda=self.use_cuda)
                anchors = block['anchors'].split(',')
                anchor_mask = block['mask'].split(',')
                yolo_layer.anchor_mask = [int(i) for i in anchor_mask]
                yolo_layer.anchors = [float(i) for i in anchors]
                yolo_layer.num_classes = int(block['classes'])
                yolo_layer.num_anchors = int(block['num'])
                yolo_layer.anchor_step = len(yolo_layer.anchors)//yolo_layer.num_anchors
                try:
                    yolo_layer.rescore = int(block['rescore'])
                except:
                    pass
                yolo_layer.ignore_thresh = float(block['ignore_thresh'])
                yolo_layer.truth_thresh = float(block['truth_thresh'])
                yolo_layer.stride = prev_stride
                yolo_layer.nth_layer = ind
                yolo_layer.net_width = self.width
                yolo_layer.net_height = self.height
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                models.append(yolo_layer)                
            else:
                print('unknown type %s' % (block['type']))
    
        return models

    def load_binfile(self, weightfile):
        fp = open(weightfile, 'rb')
       
        version = np.fromfile(fp, count=3, dtype=np.int32)
        version = [int(i) for i in version]
        if version[0]*10+version[1] >=2 and version[0] < 1000 and version[1] < 1000:
            seen = np.fromfile(fp, count=1, dtype=np.int64)
        else:
            seen = np.fromfile(fp, count=1, dtype=np.int32)
        self.header = torch.from_numpy(np.concatenate((version, seen), axis=0))
        self.seen = int(seen)
        body = np.fromfile(fp, dtype=np.float32)
        fp.close()
        return body

    def load_weights_roi(self, weightfile):
        ind = -2
        for block in self.blocks:
            ind += 1
            if block['type'] == 'net':
                continue
            elif block['type'] == 'convolutional':
                pass
            elif block['type'] == 'connected':
                pass
            elif block['type'] == 'roiclass':
                self.models[ind].load_state_dict(torch.load(weightfile+self.models[ind].class_size+".weights", map_location=lambda storage, loc: storage.cuda()))
            elif block['type'] == 'maxpool':
                pass
            elif block['type'] == 'roiclass':
                pass
            elif block['type'] == 'reorg':
                pass
            elif block['type'] == 'upsample':
                pass
            elif block['type'] == 'route':
                pass
            elif block['type'] == 'shortcut':
                pass
            elif block['type'] == 'region':
                pass
            elif block['type'] == 'yolo':
                pass                
            elif block['type'] == 'avgpool':
                pass
            elif block['type'] == 'softmax':
                pass
            elif block['type'] == 'cost':
                pass
            else:
                print('unknown type %s' % (block['type']))

    def load_weights(self, weightfile):
        buf = self.load_binfile(weightfile)

        start = 0
        ind = -2
        for block in self.blocks:
            if start >= buf.size:
                break
            ind = ind + 1
            if block['type'] == 'net':
                continue
            elif block['type'] == 'convolutional':
                model = self.models[ind]
                batch_normalize = int(block['batch_normalize'])
                if batch_normalize:
                    start = load_conv_bn(buf, start, model[0], model[1])
                else:
                    start = load_conv(buf, start, model[0])
            elif block['type'] == 'connected':
                model = self.models[ind]
                if block['activation'] != 'linear':
                    start = load_fc(buf, start, model[0])
                else:
                    start = load_fc(buf, start, model)
            elif block['type'] == 'roiclass':
                pass
            elif block['type'] == 'maxpool':
                pass
            elif block['type'] == 'roiclass':
                pass
            elif block['type'] == 'reorg':
                pass
            elif block['type'] == 'upsample':
                pass
            elif block['type'] == 'route':
                pass
            elif block['type'] == 'shortcut':
                pass
            elif block['type'] == 'region':
                pass
            elif block['type'] == 'yolo':
                pass                
            elif block['type'] == 'avgpool':
                pass
            elif block['type'] == 'softmax':
                pass
            elif block['type'] == 'cost':
                pass
            else:
                print('unknown type %s' % (block['type']))

    def save_weights_roi(self, outfile, cutoff=0):
        if cutoff <= 0:
            cutoff = len(self.blocks)-1

        dirname = os.path.dirname(outfile)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        ind = -1
        for blockId in range(1, cutoff+1):
            ind = ind + 1
            block = self.blocks[blockId]

            if block['type'] == 'convolutional':
                pass
            elif block['type'] == 'connected':
                pass
            elif block['type'] == 'roiclass':
                model = self.models[ind]
                torch.save(model.state_dict(), outfile+str(model.class_size)+".weights")
            elif block['type'] == 'maxpool':
                pass
            elif block['type'] == 'reorg':
                pass
            elif block['type'] == 'upsample':
                pass                
            elif block['type'] == 'route':
                pass
            elif block['type'] == 'shortcut':
                pass
            elif block['type'] == 'region':
                pass
            elif block['type'] == 'yolo':
                pass
            elif block['type'] == 'avgpool':
                pass
            elif block['type'] == 'softmax':
                pass
            elif block['type'] == 'cost':
                pass
            else:
                print('unknown type %s' % (block['type']))

    def save_weights(self, outfile, cutoff=0):
        if cutoff <= 0:
            cutoff = len(self.blocks)-1

        dirname = os.path.dirname(outfile)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        fp = open(outfile, 'wb')
        self.header[3] = self.seen
        header = np.array(self.header[0:3].numpy(), np.int32)
        header.tofile(fp)
        if (self.header[0]*10+self.header[1]) >= 2:
            seen = np.array(self.seen, np.int64)
        else:
            seen = np.array(self.seen, np.int32)
        seen.tofile(fp)

        ind = -1
        for blockId in range(1, cutoff+1):
            ind = ind + 1
            block = self.blocks[blockId]
            if block['type'] == 'convolutional':
                model = self.models[ind]
                batch_normalize = int(block['batch_normalize'])
                if batch_normalize:
                    save_conv_bn(fp, model[0], model[1])
                else:
                    save_conv(fp, model[0])
            elif block['type'] == 'connected':
                model = self.models[ind]
                if block['activation'] != 'linear':
                    save_fc(fp, model)
                else:
                    save_fc(fp, model[0])
            elif block['type'] == 'roiclass':
                pass
            elif block['type'] == 'maxpool':
                pass
            elif block['type'] == 'reorg':
                pass
            elif block['type'] == 'upsample':
                pass                
            elif block['type'] == 'route':
                pass
            elif block['type'] == 'shortcut':
                pass
            elif block['type'] == 'region':
                pass
            elif block['type'] == 'yolo':
                pass
            elif block['type'] == 'avgpool':
                pass
            elif block['type'] == 'softmax':
                pass
            elif block['type'] == 'cost':
                pass
            else:
                print('unknown type %s' % (block['type']))
        fp.close()

#Test Image
def resize_img(img, w, h):
    im_h, im_w, im_c = img.shape
    resized = np.zeros((h, w, im_c), dtype = np.float32)
    part = np.zeros((im_h, w, im_c), dtype = np.float32)
    w_scale = np.float32(im_w - 1) / np.float32(w - 1)
    h_scale = np.float32(im_h - 1) / np.float32(h - 1)
    sx = np.arange(w - 1) * w_scale
    ix = sx.astype(dtype = np.int)
    dx = sx - ix
    dx = dx.reshape(-1, 1)
    part[:, :w-1, :] = (1 - dx) * img[:, ix, :] + (dx) * img[:, ix + 1, :]
    part[:, w - 1, :] = img[:im_h, im_w - 1, :]

    sy = np.arange(h) * h_scale
    iy = sy.astype(dtype = np.int)
    dy = sy - iy
    dy = dy.reshape(-1, 1, 1) 
    resized = (1 - dy) * part[iy, :, :]
    sy = sy[:-1]
    iy = iy[:-1]
    dy = dy[:-1]
    resized[:-1, :, :] += (dy) * part[iy + 1, :, :]
    return resized

def letterbox_img(img, w, h):
    #print(img.shape)
    im_h, im_w, im_c = img.shape
    if (float(w)/im_w < float(h)/im_h):
        new_w = int(w)
        new_h = int((im_h * w)/im_w)
    else:
        new_h = int(h)
        new_w = int((im_w * h)/im_h)
    #print(new_w, new_h)
    resized = resize_img(img, new_w, new_h)
    rh, rw, _ = resized.shape
    boxed = np.full((h, w, im_c), fill_value = 0.5)
    iy = np.arange(rh)
    ix = np.arange(rw)
    dy = (h - new_h) // 2 
    dx = (w - new_w) // 2
    boxed[dy: dy + rh, dx: dx + rw, :] = resized
    return boxed

#Test Bench: Success
if __name__ == '__main__':
    # #Initialize the model
    darknet = Darknet('configs/yolov3_person_mini_roi.cfg')
    print(darknet) 
    # for name, params in darknet.named_parameters():
    #     if 'models.'+str(darknet.roi_index) not in name:
    #        params.requires_grad = False

    # for name, params in darknet.named_parameters():
    #     if params.requires_grad == True:
    #         print(name)
