'''
Author: Shreejal Trivedi

Date: 30th July 2020

Description: Main function call to trainer.py. Intialize the training class wrapper and start the training.

'''

from trainer import YOLOv3ROIClassTrainer
import argparse

# Testbench == SUCCESS
if __name__ == '__main__':

    print('Demo Command: python train_detector_roiclass.py -c <path-to-cfg> -d <path-to-datafile> -w <path-to-detector-weights> -wr <path-to-branch-weights> -r <reset training flag>')
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', '-d',
        type=str, default='cfg/sketch.data', help='data definition file')
    parser.add_argument('--config', '-c',
        type=str, default='cfg/sketch.cfg', help='network configuration file')
    parser.add_argument('--weights', '-w',
        type=str, help='initial weights file')
    parser.add_argument('--weights_roi', '-wr',
        type=str, help='initial weights file roi')
    parser.add_argument('--reset', '-r',
        action="store_true", default=False, help='initialize the epoch and model seen value')


    FLAGS, _ = parser.parse_known_args()

    # Initialize the class and start training by calling start_training function
    
    '''
    YOLOv3ROIClassTrainer input parameters: 
    Compulsory parameters: FLAGS
    Optional parameters: FREEZE_DETECTOR, save_interval, test_interval, evaluate, conf_thresh, nms_thresh, iou_thresh
    
    Pattern: YOLOv3ROIClass(FLAGS, ..., optional parameters, ...)
    '''
    
    trainer = YOLOv3ROIClassTrainer(FLAGS, multiscale=False)
    trainer.start_training()
