'''
Name: Shreejal Trivedi

Date: 7th September 2020

Description: BGS Class definition for integration support with YOLOv3 SST

'''


from __future__ import division
import cv2
import os
import numpy as np
import operator


class BGSRunner():

    def __init__(self, width=320, height=320, bgs_algorithm='GSOC', resize=False, learningRate=0.9, best_boxes=10, test_resolution=2 * 2, thresh_iou=0.6):

        if bgs_algorithm == 'GSOC':
            self.algo = cv2.bgsegm.createBackgroundSubtractorGSOC()
        elif bgs_algorithm == 'LSBP':
            self.algo = cv2.bgsegm.createBackgroundSubtractorLSBP()
        else:
            print('Please specify supported BGS algorithm: GSOC/LSBP')
            return
        
        self.width = width
        self.height = height
        self.resize = resize
        self.learningRate = learningRate
        self.best_boxes = best_boxes
        self.test_resolution = test_resolution
        self.thresh_iou = thresh_iou

    def apply(self, frame):

        self.detMask = np.zeros(shape=(self.width, self.height), dtype=np.uint8)
        if self.resize:
            frame = cv2.resize(frame, (self.width, self.height))

        self.fgMask = self.algo.apply(frame)
    
    def checkUnwantedBoxes(self, boxes_detections, boxes_contours):
        

        if boxes_contours.shape != (0,) and boxes_detections.shape != (0,):
            IoU = self.run_vec_IoU(boxes_contours, boxes_detections)
            flag_mask = self.check_rectangle(boxes_contours, boxes_detections)
            check_flag = np.logical_and(IoU < self.thresh_iou, np.logical_not(flag_mask))
            boxes_remaining = boxes_contours[np.where(check_flag.all(axis=1))]
        else:
            boxes_remaining = boxes_contours
        
        # Sorting the list of contours obtained after applying BGS
        list_area = dict()

        for box in boxes_remaining:
            list_area[(box[0], box[1], box[2], box[3])] = (box[2] - box[0]) * (box[3] - box[1])
        sorted_list_area = sorted(list_area.items(), key=operator.itemgetter(1), reverse=True)
        
        # Taking first N boxes sorted by area 
        if len(sorted_list_area) > self.best_boxes:
            sorted_list_area = sorted_list_area[:self.best_boxes]
        
        boxes_final = list()

        # Dump the boxes whose area is greater than test resolution
        for list_area in sorted_list_area: # Can remove the for loop by vectorizing the operation. REMAINGING
            xmin, ymin, xmax, ymax = list_area[0]
    
            if list_area[1] > (self.test_resolution):
                boxes_final.append((0, xmin, ymin, xmax, ymax))
        boxes_final = np.array(boxes_final)
        
        return boxes_final
   
    def update(self, frame):

        self.algo.updateBg(frame, self.detMask, learningRate=self.learningRate)
        self.detMask = None # Clean Detection Mask
    
    def getCountours(self):

        boxes_contours = list()
        contours, _ = cv2.findContours(self.fgMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)    
        
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            boxes_contours.append([x, y, x + w, y + h])      
        boxes_contours = np.array(boxes_contours)
        return boxes_contours
    
    def drawContours(self, img, boxes):

        for bb in boxes:
            cv2.rectangle(img, (bb[0], bb[1]), (bb[2], bb[3]), color=(255, 0, 0))
        
    def updateDetMask(self, boxes, flag=True):

        boxes = boxes[:, 1:] if boxes.shape[0] > 0 else np.array([])

        for box in boxes.astype(int):   
            self.detMask[max(box[1], 0): max(box[3], 0), max(box[0], 0): max(box[2], 0)] = 255 if flag else 127
    
    def run_vec_IoU(self, bboxes1, bboxes2):

        x11, y11, x12, y12 = np.split(bboxes1, 4, axis=1)
        x21, y21, x22, y22 = np.split(bboxes2, 4, axis=1)

        xA = np.maximum(x11, np.transpose(x21))
        yA = np.maximum(y11, np.transpose(y21))
        xB = np.minimum(x12, np.transpose(x22))
        yB = np.minimum(y12, np.transpose(y22))        

        interArea = np.maximum((xB - xA + 1), 0) * np.maximum((yB - yA + 1), 0)        
        boxAArea = (x12 - x11 + 1) * (y12 - y11 + 1)
        boxBArea = (x22 - x21 + 1) * (y22 - y21 + 1)        
        iou = interArea / (boxAArea + np.transpose(boxBArea) - interArea)        
        return iou
    
    def draw_boxes(self, img, boxes, color):
    
        for bb in boxes:
            img = cv2.rectangle(np.float32(img), (bb[1], bb[2]), (bb[3], bb[4]), color, 2)
        return img
        
    def check_rectangle(self, bboxes1, bboxes2):
        
        x11, y11, x12, y12 = np.split(bboxes1, 4, axis=1)
        x21, y21, x22, y22 = np.split(bboxes2, 4, axis=1)

        fx1 = x11 >= np.transpose(x21) 
        fy1 = y11 >= np.transpose(y21)
        fx2 = x12 <= np.transpose(x22)
        fy2 = y12 <= np.transpose(y22)

        f_dx1 = x21 >= np.transpose(x11)
        f_dy1 = y21 >= np.transpose(y11)
        f_dx2 = x22 <= np.transpose(x12)
        f_dy2 = y22 <= np.transpose(y12)

        flag_dmask = np.logical_and(np.logical_and(np.logical_and(f_dx1, f_dy1), f_dx2), f_dy2)
        
        flag_mask = np.logical_and(np.logical_and(np.logical_and(fx1, fy1), fx2), fy2)

        final_flag_mask = np.logical_or(flag_mask, np.transpose(flag_dmask))

        return final_flag_mask



        

