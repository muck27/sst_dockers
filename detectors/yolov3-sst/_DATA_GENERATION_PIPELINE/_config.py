'''
MACRO DEFINITIONS
'''

# INPUT Directories

#IMGPATH = '/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Dataset/INGRAM/images'
IMGPATH = '/images/train/'
#LABELPATH = '/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Dataset/INGRAM/labels'
LABELPATH = '/gt'
#DETSPATH = '/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Dataset/INGRAM/texts'
DETSPATH = '/dets'

# OUTPUT Directories

DEBUGPATH = '/sst/boxes'
OUTPATH = '/sst/labels'
DEBUGFLAG = True

# Paramters for Proposals

THRESH_IOU = 0.1  # Negative Boxes to be consider whose IoU between detections and GTs is less than given threshold
MAX_PROPOSALS = 10  # Maximum number of Proposals to keep per image. This applies only to Negative Proposals
TOTAL_IOU_SIZE = 5  # Total number of Positive Proposals to generate per GT.
MIN_POS_THRESH = 0.6  # Minimum IoU with which PP will be generated from generateBoundingBox() function
MIN_NEG_SIZE = 2  # Maximum negative proposals of the boxes whose IoU is less than THRESH_IOU 
MIN_NEG_THRESH = 0.3  # Minimum IOU with which NP will be generated from generteBoundingBox() function
