'''
Author: Shreejal Trivedi

Date: 1st August 2020

Description:    This is the main file to generate data for SST YOLOv3 + Branch Classifier. 
                You can change the configurations of the Proposals, Input and Output directories in
                _config.py file

'''


import numpy as np
import cv2
import os
from utils import draw_bbox, normalize_coordinates, convert_from_YOLO, get_nROI, run_vec_IoU
from tqdm import tqdm
from boundingBoxGenerator import generateBoundingBox
from _config import *
import argparse
import zmq

# Make output directories mentioned in _config file if not present


parser = argparse.ArgumentParser()
parser.add_argument('--port', '-p', type=int,default=1, help='Flag for port')
parser.add_argument('--imgpath', '-i', type=str,default='/images/train', help='Flag for port')
FLAGS, _ = parser.parse_known_args()

port = FLAGS.port
ImgPath = FLAGS.imgpath

if not os.path.exists(DEBUGPATH):
    os.makedirs(DEBUGPATH)

if not os.path.exists(OUTPATH):
    os.makedirs(OUTPATH)

labels = os.listdir(LABELPATH)

positives = 0
negatives = 0


context = zmq.Context()
socket = context.socket(zmq.PAIR)
socket.bind("tcp://*:%s" % str(port))

start_process = False

while start_process == False:
    if socket.recv() == 'start container':
        start_process = True
        print('hello')

for label in tqdm(labels):

    data = np.loadtxt(os.path.join(LABELPATH, label))
    data = data if data.ndim > 1 else data.reshape(1, -1)
    
    data = data[np.where(data[:, 0] == 0)[0]] # Read only Person Class from the labelfiles

    if data.shape[0] > 0:
        
        imgpath = os.path.join(ImgPath, label.replace('txt', 'jpg')) # Creates Bottleneck
        img = cv2.imread(imgpath)
        width, height = img.shape[1], img.shape[0]

        IoU_list = np.random.uniform(MIN_POS_THRESH, 1.0, size=TOTAL_IOU_SIZE)
        rect_data = convert_from_YOLO(data[:, 1:], width, height)
        rect_data = rect_data[np.where((rect_data[:, 3] - rect_data[:, 1] != 0) & (rect_data[:, 2] - rect_data[:, 0] != 0))[0]]
        pRoI = generateBoundingBox(rect_data, IoU_list, [width, height])
        
        total_pRoI = np.vstack((rect_data, pRoI))

        # Read Detections

        nRoI = np.array([])
        if os.path.getsize(os.path.join(DETSPATH, label)) > 0:
            dets = np.loadtxt(os.path.join(DETSPATH, label))
            dets = dets if dets.ndim > 1 else dets.reshape(1, -1)
            rect_dets = dets[:, 2:]
            iou = run_vec_IoU(total_pRoI, rect_dets)
            valid_idxs = np.where((iou < THRESH_IOU).all(axis=0))[0]
            valid_rect_dets = rect_dets[valid_idxs]

            if valid_rect_dets.shape[0] > 0:          
                
                IoU_array = np.random.uniform(MIN_NEG_THRESH, 1.0, size=MIN_NEG_SIZE)
                valid_rect_dets = valid_rect_dets[np.where((valid_rect_dets[:, 3] - valid_rect_dets[:, 1] != 0) & (valid_rect_dets[:, 2] - valid_rect_dets[:, 0] != 0))[0]]
                nRoI = generateBoundingBox(valid_rect_dets, IoU_list, [width, height])
        
        total_nRoI = nRoI.shape[0]
        
        if total_nRoI < MAX_PROPOSALS:
            to_generate = MAX_PROPOSALS - total_nRoI
            sampled = get_nROI(total_pRoI, width, height, numROIs=to_generate)
            if nRoI.shape[0] > 0:
                nRoI = np.vstack((nRoI, sampled))
            else:
                nRoI = sampled
        else: # GIVE PRIORITY TO THE DETECTION NEGATIVE ROIS
            idxs = np.arange(0, nRoI.shape[0])
            np.random.shuffle(idxs)
            nRoI = nRoI[idxs[:MAX_PROPOSALS]]
        
        if DEBUGFLAG:
            draw_bbox(img, rect_data, color=(0, 255, 0))
            draw_bbox(img, pRoI, color=(255, 0, 0))
            draw_bbox(img, nRoI, color=(0, 0, 255))
            cv2.imwrite(os.path.join(DEBUGPATH, os.path.basename(imgpath)), img)

        # Generate Labels for GTs, Positve and Negative ROIs

        label_gt = np.zeros(shape=(rect_data.shape[0], 1))
        label_proi = np.ones(shape=(pRoI.shape[0], 1))

        final_gt = np.hstack((label_gt, rect_data))
        final_proi = np.hstack((label_proi, pRoI))

        data_prop = np.vstack((final_gt, final_proi))

        if nRoI.shape[0] > 0:
            label_nroi = np.negative(np.ones(shape=(nRoI.shape[0], 1)))
            final_nroi = np.hstack((label_nroi, nRoI))
            data_prop = np.vstack((data_prop, final_nroi))

        positives += final_proi.shape[0]
        negatives += final_nroi.shape[0]

        data_prop = normalize_coordinates(data_prop, width, height)
        np.savetxt(os.path.join(OUTPATH, label), data_prop, fmt='%d %.2f %.2f %.2f %.2f')

print("Positive ROIs: %d | Negative ROIs : %d" %(positives, negatives))

socket.send('completed')

       
        


        
        





        



