## Data Generation Steps ##

This folder contains some main and utility functions to generate the data for training **SST YOLOv3 + Branch Classifier**. All the configurable paramters are
present in `_config.py` file. Follow the steps given below to successfully generate the data. 

___

### Pre-requisites to run the data generation pipeline ###

- Image folder of the data
- Annotations of the images in `.txt` file. Format should be `class_id x y w h` normalized coordinates (or YOLO format).
- Detection files (`.txt` files) of the images after running a detector on a very low threshold like `thresh = 0.005`. Format: `class_id conf TLx TLy BRx BRy`. 

___

### About the python scripts ###

- `boundingBoxGenerator.py`: This script contains the algorithmic implementation of the paper [Generating Positive Bounding Boxes for Balanced Training of Object Detectors](https://arxiv.org/abs/1909.09777)
- `utils.py`: This script contains some useful snippets and logic to generate both negative and positive proposals. Generation of PPs from this script can also be used in replacement of the above snippet.
- `_config.py`: This have macro definitions of the tunable paramters for data generation.
- `data_generator.py`: Main file call to generate data.

___

### Setting up tuning parameters for data pipeline ###

Main configuration file is `_config.py`. All the parameters and their usage is mentioned below. Tune the given parameters in accordance of your requirements. 
This macro difinitions are taken by `data_generator.py` file for futher processing. 

```
- IMGPATH: Path to the image folder of your data

- LABELPATH: Path to the annotations folder of your data. Please note that the data should be in a format mentioned in the pre-requisite section.

- DETSPATH: Path to the detections folder of your data. Please note that the data should be in a formate mentioned in the pre-requisite section.

- DEBUGPATH: Path to dump the debug images. These images containes the bounding boxes around the GTs, Positive and Negative Proposals. 
           
           Ground Truths: Green Color
           
           Positive Proposals: Blue Color
           
           Negative Proposals: Red Color
           
- OUTPATH: Path to dump the final label files. 
         
           Format of the given .txt files:
         
           0 / 1 / -1 x y w h (Normalized Coordinates)
         
           0: Ground Truth for Detector
           1: Positive Proposal of GT
           -1: Negative Proposals
         
           For example: frame_0.txt
         
           0 0.5 0.3 0.02 0.01
           1 0.49 0.28 0.01 0.01
           -1 0.9 0.6 0.2 0.3
         
          NOTE: During Training of Branch Classifier, only 1 and -1 labels will be taken into consideration. 0's are only for the detector training.


- DEBUGFLAG: Flag to turn debug image dump ON or OFF. If True, debug images will be dumped in DEBUGPATH and vice-versa.

- THRESH_IOU: Negative Boxes to be consider whose IoU between detections and GTs is less than given threshold

- MAX_PROPOSALS: Maximum number of Proposals to keep per image. This applies only to Negative Proposals

- TOTAL_IOU_SIZE: Total number of Positive Proposals to generate per GT.

- MIN_POS_THRESH: Minimum IoU with which PPs will be generated from generateBoundingBox() function

- MIN_NEG_SIZE: Maximum negative proposals of the boxes whose IoU is less than THRESH_IOU 

- MIN_NEG_THRESH: Minimum IOU with which NPs will be generated from generteBoundingBox() function
         
```

**DEFAULT PARAMS**

```

# INPUT Directories

IMGPATH = '/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Dataset/INGRAM/images'
LABELPATH = '/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Dataset/INGRAM/labels'
DETSPATH = '/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Dataset/INGRAM/texts'

# OUTPUT Directories

DEBUGPATH = 'boxes'
OUTPATH = 'labels'
DEBUGFLAG = True

# Paramters for Proposals

THRESH_IOU = 0.1  
MAX_PROPOSALS = 10  
TOTAL_IOU_SIZE = 5  
MIN_POS_THRESH = 0.6  
MIN_NEG_SIZE = 2  
MIN_NEG_THRESH = 0.3  

```
___


Command to run: `$python data_generator.py`

___

**SOME OBSERVATIONS AND CONCLUSIONS**

1. Branch Classifier is a very shallow CNN that can learn varied features of a particular site with a very less data. During testing and ablation studies, we got to know that even with very
   less images viz. 500 images can perform at par with the large corpus. Based on the tunable parameters mentioned above, we can get enough data to train the shallow network. You can consider this 
   pipeline as the amplification of the original annotatations, by taking into consideration the scale and the count of the GTs.

2. Pre-requisite step contains detection path which contains the detections on that particular site. This folder is basically used to generate the negative proposals in a smart manner. You can also generate NPs in 
   random fashion, but the former method improved the accuracy of the branch detector to a large extent(From 92.71 to 96.67). 

___

**Create an issue, if you face any bugs or irrelavancies during the generation of data **



