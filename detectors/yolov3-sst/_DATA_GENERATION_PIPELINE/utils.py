'''
Name: Shreejal Trivedi

Date: 3rd April, 2020

Description: This script contain utility routines to generate postive and negative ROIs(Region of Interest) given IoU, Ground Truths and number of ROIs to return. 

'''
from __future__ import division
import cv2
import numpy as np
import math
import cv2

global dict_grids
dict_grids = dict()


def convert_from_YOLO(bbox, width, height):
    xc, yc, w, h = np.split(bbox, 4, axis=1)

    xmin = (xc * width)  - (w * width) / 2.    
    xmax = xmin + (w * width)

    ymin = (yc * height)  - (h * height) / 2.    
    ymax = ymin + (h * height)

    return np.hstack((xmin, ymin, xmax, ymax))

def normalize_coordinates(bbox, width, height):
    
    _, x1, y1, x2, y2 = np.split(bbox, 5, axis=1)
    x_center = ((x1 + x2) / 2.) / float(width)
    y_center = ((y1 + y2) / 2.)/ float(height)

    w = (x2 - x1) / float(width)
    h = (y2 - y1) / float(height)
    
    return np.hstack((_, x_center, y_center, w, h))

def draw_bbox(image, bb, color=(255, 0, 0)):
    bb = bb.astype(int)
    for b in bb:
        cv2.rectangle(image, (b[0], b[1]), (b[2], b[3]), color, 1)


def run_vec_IoU(bboxes1, bboxes2):
    '''

    Input: bboxes1[Ground Truths/Generated Boxes] [NX4] and bboxes2[Ground Truth/Generated Boxes] [MX4] 
    Input format of boxes should be [TLx, TLy, BRx, BRy]

    Output: IoU [N X M] matrix containing N --> M IoUs

    Purpose: Used for DEBUG

    '''
    
    x11, y11, x12, y12 = np.split(bboxes1, 4, axis=1)
    x21, y21, x22, y22 = np.split(bboxes2, 4, axis=1)

    xA = np.maximum(x11, np.transpose(x21))
    yA = np.maximum(y11, np.transpose(y21))
    xB = np.minimum(x12, np.transpose(x22))
    yB = np.minimum(y12, np.transpose(y22))

    interArea = np.maximum((xB - xA + 1), 0) * np.maximum((yB - yA + 1), 0)

    boxAArea = (x12 - x11 + 1) * (y12 - y11 + 1)
    boxBArea = (x22 - x21 + 1) * (y22 - y21 + 1)

    iou = interArea.astype(float) / (boxAArea + np.transpose(boxBArea) - interArea)

    return iou

def get_rectangle_intersection_info(bboxes):

    '''
    Input: bboxes[Any rectangular boxes] [NX4]
    Input Format: [[TLx, TLy, BRx, BRy], ...] 

    Output: Boxes which are not intersecting with any box from the given list

    Purpose: To get Negative ROIs

    '''

    info_flag = np.ones(shape=(bboxes.shape[0], bboxes.shape[0]), dtype=np.int8)    
    x1, y1, x2, y2 = np.split(bboxes, 4, axis=1)
    
    ansx = x1 >= np.transpose(x2)
    ansy = y1 <= np.transpose(y2)
    info_flag[np.where(np.logical_or(ansx, np.transpose(ansx)) == True) or np.where(np.logical_or(ansy, np.transpose(ansy)) == True)] = 0

    return bboxes[info_flag.sum(axis=1) == 1]
   
def get_pROI(bbox, width, height, numROIs=1, IoU=0.7):
    
    bbox_proi = vectorized_get_rois(bbox, width, height, IoU)

    if numROIs == 1:
        return bbox_proi
    else:
        for i in range(numROIs):
            bbox_roi_cnt = vectorized_get_rois(bbox, width, height, IoU)
            bbox_proi = np.vstack((bbox_proi, bbox_roi_cnt))

        return bbox_proi

def vectorized_get_rois(bbox, width, height, IoU=0.7):#Generates 1 positive RoI per box

    def get_new_roi(x1, y1, x2, y2, kx, ky, width, height):
        p = np.random.uniform()
        if p <= 0.25:#First Quadrant
            x1_new = (x1.reshape(-1, 1) + kx).clip(min=0, max=width)
            y1_new = (y1.reshape(-1, 1) + ky).clip(min=0, max=height)
            x2_new = (x2.reshape(-1, 1) + kx).clip(min=0, max=width)
            y2_new = (y2.reshape(-1, 1) + ky).clip(min=0, max=height)

        elif p > 0.25 and p <= 0.5:#Second Quadrant
            x1_new = (x1.reshape(-1, 1) - kx).clip(min=0, max=width)
            y1_new = (y1.reshape(-1, 1) + ky).clip(min=0, max=height)
            x2_new = (x2.reshape(-1, 1) - kx).clip(min=0, max=width)
            y2_new = (y2.reshape(-1, 1) + ky).clip(min=0, max=height)

        elif p > 0.5 and p <= 0.75:# Third Quadrant
            x1_new = (x1.reshape(-1, 1) - kx).clip(min=0, max=width)
            y1_new = (y1.reshape(-1, 1) - ky).clip(min=0, max=height)
            x2_new = (x2.reshape(-1, 1) - kx).clip(min=0, max=width)
            y2_new = (y2.reshape(-1, 1) - ky).clip(min=0, max=height)
        else: #Fourth Quadrant
            x1_new = (x1.reshape(-1, 1) + kx).clip(min=0, max=width)
            y1_new = (y1.reshape(-1, 1) - ky).clip(min=0, max=height)
            x2_new = (x2.reshape(-1, 1) + kx).clip(min=0, max=width)
            y2_new = (y2.reshape(-1, 1) - ky).clip(min=0, max=height)        

        bbox_roi = np.hstack((x1_new, y1_new, x2_new, y2_new))
        return bbox_roi
    
    x1, y1, x2, y2 = bbox[:, 0], bbox[:, 1], bbox[:, 2], bbox[:, 3]
    areas = np.reshape((x2 - x1) * (y2 - y1), (-1, 1))
    kx = np.array((x2 - x1) - ((2 * IoU * (x2 - x1)) / (IoU + 1)))
    kx_idx = np.array([np.random.randint((i + 1), size=1) for i in kx.astype(int)])
    ky = (areas - (2 * areas * IoU / (1 + IoU)) - (kx_idx * (y2 - y1).reshape(-1, 1))) / ((x2 - x1).reshape(-1, 1) - kx_idx) 
    ky_idy = np.array([np.random.randint((j + 1), size=1) for i in ky for j in i])
    bbox_roi = get_new_roi(x1, y1, x2, y2, kx_idx, ky_idy, width, height)

    return bbox_roi

def get_nROI(gt, img_width, img_height, numROIs=10, thresh_IoU=0.):#Returns Negative ROIs 

    valid_anchors, ious = generate_anchors(img_width, img_height, gt, get_area(gt))
    valid_anchors_idxs = np.where((ious <= thresh_IoU).all(axis=0))[0]
    
    if valid_anchors_idxs.shape[0] > 0:
        rand_list = np.random.randint(low=0, high=len(valid_anchors_idxs), size=numROIs)
        valid_anchors = valid_anchors[valid_anchors_idxs[rand_list]]
        return valid_anchors
    else:
        return np.array([])

def generate_anchors_at_pixel(ratios, anchor_scales, ctr_x, ctr_y):
    
    anchor_base = np.zeros((len(ratios) * len(anchor_scales), 4))
    for i in range(len(ratios)):
        for j in range(len(anchor_scales)):
            h = anchor_scales[j] * np.sqrt(ratios[i])
            w = anchor_scales[j] * np.sqrt(1./ ratios[i])
            index = i * len(anchor_scales) + j
            anchor_base[index, 0] = ctr_x - w / 2.
            anchor_base[index, 1] = ctr_y - h / 2.
            anchor_base[index, 2] = ctr_x + w / 2.
            anchor_base[index, 3] = ctr_y + h / 2.   
    return anchor_base

def get_area(boxes):
    area = [(box[2] - box[0]) * (box[3] - box[1]) for box in boxes]
    return np.array(area)

def generate_anchors(img_width, img_height, gt, area):

    area_sample = math.sqrt(np.mean(area))
    anchor_scales = [area_sample, area_sample * 1.25, area_sample * 1.5]
    sub_sample = 32
    ratios = [0.5, 1, 2]

    fe_size_x = img_width // sub_sample
    fe_size_y = img_height // sub_sample

    ctr_x = np.arange(sub_sample, (fe_size_x + 1) * sub_sample, sub_sample)
    ctr_y = np.arange(sub_sample, (fe_size_y + 1) * sub_sample, sub_sample)

    anchors = np.zeros((fe_size_x * fe_size_y, len(ratios) * len(anchor_scales), 4))
    index = 0

    for y in ctr_y:
        for x in ctr_x:
            anchor_base = generate_anchors_at_pixel(ratios, anchor_scales, x, y)   
            anchors[index, :] = anchor_base
            index += 1

    anchors = anchors.reshape(fe_size_x * fe_size_y * len(ratios) * len(anchor_scales), 4)
    valid_anchors_idxs = np.where((anchors[:, 0] >= 0) & (anchors[:, 1] >= 0) & (anchors[:, 2] <= img_width) & (anchors[:, 3] <= img_height))
    valid_anchors = anchors[valid_anchors_idxs]
    ious = run_vec_IoU(gt, valid_anchors)

    return valid_anchors, ious

# TESTBENCH = SUCCESS
 
if __name__ == '__main__':
    img = np.zeros((800, 800, 3), dtype = np.uint8)
    gt  = np.array([[20, 30, 400, 500], [300, 300, 400, 250], [600, 600, 700, 700], [550, 550, 600, 600], [510, 510, 540, 540]])
    print(get_nROI(img.shape[1], img.shape[0], gt))
