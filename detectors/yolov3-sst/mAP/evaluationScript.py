'''
Author: Shreejal Trivedi

Date: 24-04-2020

Description: Calculation of mAP on COCO and Pascal VOC 2012 Standards

'''

from __future__ import division
import numpy as np
import os
import argparse
from copy import deepcopy
import time
import json
from collections import OrderedDict
import zmq 
import random
import time

#Classes taken into consideration for evaluation

global classes
classes = dict()

def vecIoU(bbox1, bbox2):
    '''
    Input: Ground Truth and Detection Boxes
           Format: [TLX, TLy, BRx, BRy] GT BOXES Shape | bbox1 ::: NX4, DETECTION BOXES Shape | bbox2 ::: MX4 

    Output: IoU Matrix Shape | NXM

    Description: Calculate vectorized IoU between ground truths and detections.
    '''

    x11, y11, x12, y12 = np.split(bbox1, 4, axis=1)
    x21, y21, x22, y22 = np.split(bbox2, 4, axis=1)

    xA = np.maximum(x11, np.transpose(x21))
    yA = np.maximum(y11, np.transpose(y21))
    xB = np.minimum(x12, np.transpose(x22))
    yB = np.minimum(y12, np.transpose(y22))        

    interArea = np.maximum((xB - xA + 1), 0) * np.maximum((yB - yA + 1), 0)        
    boxAArea = (x12 - x11 + 1) * (y12 - y11 + 1)
    boxBArea = (x22 - x21 + 1) * (y22 - y21 + 1)        
    iou = interArea / (boxAArea + np.transpose(boxBArea) - interArea)        
    return iou

def getmAPSingle(gtBox, detBox, IoU):

    '''
    Input: Ground Truth and Detection Boxes
           Format: [TLX, TLy, BRx, BRy] GT BOXES Shape | bbox1 ::: NX4, DETECTION BOXES Shape | bbox2 ::: MX4 

    Output: Mask of Detections ::: TP - True  / FP - False

    Description: Calculate mask of boxes i.e calculate TPs and FPs

    '''
    detMask = np.zeros(detBox.shape[0], dtype=np.bool)
    
    if gtBox.shape[0] == 0:
        return detMask
    
    iou = vecIoU(gtBox, detBox)
    iouValid = iou[np.where(iou > IoU)]
    sortiouValid = np.argsort(iouValid)[::-1]
    gtIdx, detIdx = np.where(iou > IoU)
  
    gtIdxValid, detIdxValid = list(), list()
    for idx in sortiouValid:
        if (gtIdx[idx] not in gtIdxValid) and (detIdx[idx] not in detIdxValid):
            gtIdxValid.append(gtIdx[idx])
            detIdxValid.append(detIdx[idx])
            detMask[detIdx[idx]] = 1
 
    return detMask
    

def getMaskBoxes(gtBoxes, detBoxes, detScores, IoU): 

    '''
    Input: Ground Truth and Detection Boxes
           Format: [TLX, TLy, BRx, BRy] GT BOXES Dictionary | bbox1 ::: NX4, DETECTION BOXES Dictionary | bbox2 ::: MX4, DETECTION SCORES Dictionary

    Output: Dictionary mask of detections ::: TP - True  / FP - False | Key ::: Image Name | Value ::: Mask Array

    Description: Calculate mask of boxes i.e calculate TPs and FPs

    '''
    
    maskBoxes = dict()
    for imgID in detBoxes.keys():
        gtBox = gtBoxes[imgID]
        if detBoxes[imgID].shape[0] > 0:
            detBox = detBoxes[imgID]
            maskBoxes[imgID] = getmAPSingle(gtBox, detBox, IoU)
        else:
            maskBoxes[imgID] = np.array([])
    return maskBoxes

def calAveragePrecision(precision, recall, points=0):

    '''
    Input: List of Precision/ Recall | Points = 0/11/101

    Output: Average Precision 

    Description: Calculate Precision and Recall based on PR Curve

    '''

    mprecision = np.concatenate(([0.], precision, [0.]))
    mrecall = np.concatenate(([0.], recall, [1.]))
    mprecision = np.flip(np.maximum.accumulate(np.flip(mprecision)))

    if points == 0: # Continues interpolation
        idxs = np.where(mrecall[1:] != mrecall[:-1])[0]
        averagePrecision = np.sum((mrecall[idxs + 1] - mrecall[idxs]) * mprecision[idxs + 1])

    elif points == 11: # 11 points sampling interpolation Pascal VOC 2012
        idxs = np.linspace(0, 1, 11)  
        averagePrecision = np.trapz(np.interp(idxs, mrecall, mprecision), idxs) 
    
    elif points == 101: # 101 points sampling interpolation COCO
        idxs = np.linspace(0, 1, 101)  
        averagePrecision = np.trapz(np.interp(idxs, mrecall, mprecision), idxs) 
    
    return averagePrecision


def calAPClass(gtBoxes, detBoxes, detScores, IoU=0.5, Confidence=0.5, points=0):

    '''
    Input: Ground Truth and Detection Boxes
           Format: [TLX, TLy, BRx, BRy] GT BOXES Dictionary | bbox1 ::: NX4, DETECTION BOXES Dictionary | bbox2 ::: MX4, DETECTION SCORES Dictionary
                   IoU | Confidence | points

    Output: Calculate AP of a particular class

    Description: Calculate mask of boxes i.e calculate TPs and FPs
    '''

    maskBoxes = getMaskBoxes(gtBoxes, detBoxes, detScores, IoU)

    totalGTs = sum([x.shape[0] for _, x in gtBoxes.items()])
    totalDets = sum([x.shape[0] for _, x in detBoxes.items()])

    scoresList = list()
    masksList = list()

    for imgID in detBoxes.keys():
        scoresList.append(detScores[imgID].tolist())
        masksList.append(maskBoxes[imgID].tolist())

    scoresList = np.array(sum(scoresList, []))
    masksList = np.array(sum(masksList, []))

    #Check assertion of masks and scores
    assert(len(scoresList) == len(masksList))

    sortedIdxs = np.argsort(-scoresList)
    scoresList = scoresList[sortedIdxs]
    masksList = masksList[sortedIdxs]
    
    truePostive = np.cumsum(masksList)
    falsePositive = np.cumsum(1-masksList)

    precision = truePostive / (truePostive + falsePositive + 1e-7)
    recall = truePostive / (totalGTs + 1e-7)

    preConf = np.interp(-Confidence, -scoresList, precision)
    recConf = np.interp(-Confidence, -scoresList, recall)

    averagePrecision = calAveragePrecision(precision, recall, points)
    
    return {'AP' : averagePrecision, 'Precision': preConf, 'Recall': recConf, 'PrecisionList': precision, 'RecallList': recall, 'totalGTs': totalGTs, 'totalDets': totalDets}

   

def getFiles(gt, det):
    '''
    Input: Path of ground truths and detections. gt | det
    
    Output: list of ground truths and detections files

    '''
    return os.listdir(gt), os.listdir(det)

def preprocessData(gtPath, detPath, gtFiles, detFiles):

    '''
    Input: Ground Truth Path ::: gtPath | Detections path ::: detPath | Ground Truth Files ::: gtFiles | Detection Files ::: detFiles

    Output: gtBoxes, detBoxes, detScores
            Format gtBoxes ::: {'filename1.txt': [[TLx, TLy, BRx, BRy], ...]} | detBoxes ::: {'filename1.txt': [[PTLx, PTLy, PBRx, PBRy], ...]} 
                  detScores: {'filename1.txt': [[PS1, PS2,...]]}

    Description: Processes the data and stores the ground truth and detections results in a dictionary.

    '''

    #Get total classes
    numClasses = len(classes.keys())

    #Make Dictionary of GroundTruths
    gtBoxes = [{} for _ in range(numClasses)]
    for filename in gtFiles:
        anns = np.loadtxt(os.path.join(gtPath, filename)).astype(int)
        anns = anns if anns.ndim > 1 else anns.reshape(1, -1)
        for i in range(numClasses):
            dict_temp = {}

            dict_temp[filename] = anns[np.where(anns[:, 0] == i)][:, 1:]
            gtBoxes[i].update(dict_temp)
    
    print("Processed GTs..!\n")

    #Make Dictionary of Detections
    detBoxes = [{} for _ in range(numClasses)]
    detScores = [{} for _ in range(numClasses)] 
    for filename in gtFiles:
        
        if os.path.getsize(os.path.join(detPath, filename)) == 0:
            anns = np.array([])
        else:
            anns = np.loadtxt(os.path.join(detPath, filename))
          
        if anns.shape[0] == 0:
            for i in range(numClasses):
                dict_temp = {}
                dict_temp[filename] = np.array([])
                detBoxes[i].update(dict_temp)
                detScores[i].update(dict_temp)
        else:
            anns = anns if anns.ndim > 1 else anns.reshape(1, -1)
            for i in range(numClasses):
                dict_temp_bbox, dict_temp_score = {}, {}
                dict_temp_bbox[filename] = anns[np.where(anns[:, 0] == i)][:, 2:]
                dict_temp_score[filename] = np.round(anns[np.where(anns[:, 0] == i)][:, 1], 2)
                detBoxes[i].update(dict_temp_bbox)
                detScores[i].update(dict_temp_score)    
    
    print("Processed Detections..!\n")

    return gtBoxes, detBoxes, detScores

def logging(message):

    print('%s' % message)

def savelog(message, flag=True):

    with open("evaluationLog.txt", "a") as f:
        f.write('%s' % message)
    f.close()
    if flag:
        logging(message)


def generateNamesDict(filename):

    '''
    Input: filename ::: File containing names of classes 
    
    Output: Dictionary of classes with index as key and name as value

    Description: Generates dictionary of classes for individual mAP calculations.

    '''

    global classes
    with open(filename, "r") as f:
        for idx, name in enumerate(f):
            classes[idx] = name

def main(args):
    savelog("Evaluating Mode: %s"%args.instance)

    #Generate Names Dictionary
    print("Generating Class Dictionary..\n")
    generateNamesDict(args.names)

    apList = list()
    numClasses = len(classes.keys())

    savelog("Model Evaluation  on PascalVOC Standards\n")
    savelog("Total Number of Classes: %d\n"%numClasses)

    gtFiles, detFiles = getFiles(args.gt, args.det)
    
    gtBoxes, detBoxes, detScores = preprocessData(args.gt, args.det, gtFiles, detFiles)

    print('Total Files ::: GTS: %d DET: %d'%(len(gtFiles), len(detBoxes[0].keys())))
    
    # On PascalVOC Standards @0.5 IoU

    tmp = {}
    tmp['ap'] = []


    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.bind("tcp://*:%s" % str(args.port))

    start = False
    while start==False:
        command = str(socket.recv().decode('utf-8'))
        print(command)
        if command=='start container':
            start = True
    for i in range(numClasses):

        tmp2 ={}
        dataAP = calAPClass(gtBoxes[i], detBoxes[i], detScores[i], IoU=args.iou, Confidence=args.confidence, points=args.points)
        savelog("Total Ground Truths Class %s ::%d\n"%(classes[i], dataAP['totalGTs']))
        savelog("Total Detections Class %s :: %d\n"%(classes[i], dataAP['totalDets']))
        savelog("AT ARGS IoU@%.2f\n"%args.iou)
        savelog("AP :: Class %s@%.2f ::: %.3f\n"%(classes[i], args.iou, dataAP['AP']))
        savelog("Precision :: Class %s@%.2f confidence ::: %.6f\n"%(classes[i], args.confidence, dataAP['Precision']))
        savelog("Recall :: Class %s@%.2f confidence ::: %.6f\n\n"%(classes[i], args.confidence, dataAP['Recall']))
        if dataAP['totalGTs'] > 0:
            apList.append(dataAP['AP'])

        tmp2['class'] = classes[i].strip()
        tmp2['ap'] = dataAP['AP']
        tmp2['precision'] = dataAP['Precision']
        tmp2['recall']   = dataAP['Recall']
        tmp['ap'].append(tmp2)
      

    savelog("---------------------\n")
    savelog("mAP of Model ::: %.3f\n" % (np.sum(np.array(apList)) / len(apList)))
    savelog("---------------------\n")
    savelog("\n\n")
    tmp['model_map'] = str( (np.sum(np.array(apList)) / len(apList)))

    with open(args.mapLoc, 'w') as outfile:
        json.dump(tmp, outfile)

    socket.send_string('completed')

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--det", type=str, default="detection-results", help="Full path to detection results folder")
    parser.add_argument("-g", "--gt", type=str, default="ground-truths", help="Full path to ground truth folder")
    parser.add_argument("-i", "--iou", type=float, default=0.5, help='Calculate AP at a particular IoU')
    parser.add_argument("-p", "--points", type=int, default=0, help='Interpolation value: 0: Continues / 11: PascalVOC2012 Challenge')
    parser.add_argument("-n", "--names", default="model.names", help="Full path of file containing names of classes index wise")
    parser.add_argument("-c", "--confidence", default=0.5, type=float, help="Confidence at which Precision/Recall is calculated")
    parser.add_argument("-in", "--instance", default="DETECTION_MODEL", help="Name of the instance/model(Seprated by underscore)")
    parser.add_argument("-port", "--port", default=0, help="port for communication")
    parser.add_argument("-mapLoc", "--mapLoc", default=0, help="location of json for map")
 

    args = parser.parse_args()
    main(args)
