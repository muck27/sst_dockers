'''
Author: Shreejal Trivedi

Date: 21st August 2020

Description: Getting Classification Report on the Validation Data. 

'''

import torch
import torch.optim as optim
import torch.nn as nn
from torchvision import datasets, transforms
import gc
import dataset
from utils import *
from image import correct_yolo_boxes, draw_bbox
from cfg import parse_cfg
from darknet_v2 import Darknet
import argparse
import itertools

class YOLOv3ROICLassValidator():

    def __init__(self, FLAGS, use_cuda=None):

        ''' 
            Description: Initialization Configurations of the ROICLASS Trainer
            
                        Compulory argument: FLAGS contains netowork initializations and configuration parameters such as cfgfile, datafile, etc.
                        Optional arguments: Can be passed and initialized according to the needs and site specified.
                        
        '''

        self.datacfg = FLAGS.data
        self.cfgfile = FLAGS.config
        self.weightfile = FLAGS.weights
        self.weightfile_roi = FLAGS.weights_roi

        # Parse data options

        data_options = read_data_cfg(self.datacfg)
        self.testlist = data_options['valid']
        self.gpus = data_options['gpus']
        self.ngpus = len(self.gpus.split(','))
        self.num_workers = int(data_options['num_workers'])

        self.use_cuda = torch.cuda.is_available() and (True if use_cuda is None else use_cuda)
        self.device = torch.device('cuda' if self.use_cuda else 'cpu')
        print(self.use_cuda)
        self.outputdir = 'validation_results'

        if not os.path.exists(self.outputdir):
            os.mkdir(self.outputdir)

        self.seed = int(time.time())
        torch.manual_seed(self.seed)
        if use_cuda:
            os.environ['CUDA_VISIBLE_DEVICES'] = self.gpus
            torch.cuda.manual_seed(self.seed)
        
        # Initialize YOLOv3 + ROIClass Model
        self.initialize_model()

        # Load TestList
        self.load_testlist()

    def initialize_model(self):

            '''
            Description: Initialization of the branch + detector network
                        Contains some prerequisite function calls/ declarations for the initialization
                        of the model. 
            '''

            # Declare YOLO Model

            self.model = Darknet(self.cfgfile, use_cuda=self.use_cuda)

            # Load Pretrained YOLO weights and ROIClass weights if any
            
            if self.weightfile is not None:
                self.model.load_weights(self.weightfile)
            
            self.roi_layers = self.model.roi_layers

            if len(self.model.roi_layers) > 0 and self.weightfile_roi is not None:
                print('Loading Pretrained ROI Weights')
                self.model.load_weights_roi(self.weightfile_roi)

            # Transfer the model to GPU if cuda flag is True

            if self.use_cuda:
                if self.ngpus > 1:
                    self.model = torch.nn.DataParallel(self.model).to(self.device)
                else:
                    self.model = self.model.to(self.device)
        
    def load_testlist(self):
            
            '''
            Description: Data Loader for validation of the model. Called in test() function

            '''

            init_width = self.model.width
            init_height = self.model.height
            print("Test loader getting ready..!")
            kwargs = {'num_workers': self.num_workers, 'pin_memory': True} if self.use_cuda else {}
            self.test_loader = torch.utils.data.DataLoader(
                dataset.listDataset(self.testlist, shape=(init_width, init_height),
                            shuffle=False,
                            transform=transforms.Compose([
                                transforms.ToTensor(),
                            ]), train=False),
                batch_size=1, shuffle=False, **kwargs)
        
    def curmodel(self):
    
        if self.ngpus > 1:
            cur_model = self.model.module
        else:
            cur_model = self.model

        return cur_model

    def validate(self):

       
        ''' 
        Description: Validation Function for ROIClass + Detector Network.
                     Detector validation is only done if the main network branch is unfreezed
                     All the logs of the validation are written in savelog.txt

        '''

        self.model.eval()
        eps = 1e-5
        print('Test Module Started. Resolution: [%dx%d]. Generating Test Confusion Matrix...' %(self.model.width, self.model.height))

        # ROIClass Accuracy Calculation
        truth_list = [list() for _ in range(len(self.roi_layers))]
        pred_list = [list() for _ in range(len(self.roi_layers))]

        with torch.no_grad():

            for idx, (data, _, _, _, imgname, target_prop, prop) in enumerate(self.test_loader):
                data = data.to(self.device)
                if (self.model.roi_layers) > 0:
                   
                    _, output_fm = self.model(data)

                # For ROI CLASS
                if len(self.roi_layers) > 0: # ROIClass Validation

                    prop = torch.reshape(prop, (prop.shape[0] * prop.shape[1], 5))
                    target_prop = torch.reshape(target_prop, (target_prop.shape[0] * target_prop.shape[1], 1))
                    
                    for i, l in enumerate(self.roi_layers):
                                            
                        prop_filters, target_prop_filters = l.candidate_proposals(prop, target_prop)
                        
                        if prop_filters.shape[0] > 0:
                            
                            out = l(output_fm[i]['x'], prop_filters)
                            pred = np.argmax(out.data.cpu().numpy(), axis=1)
                            truth = target_prop_filters.cpu().numpy().astype(int).reshape(-1)
                            pred_list[i].append(pred)
                            truth_list[i].append(truth)

                            img = transforms.ToPILImage()(data[0].cpu())
                            props = prop.cpu().numpy()[:, 1:]
                            tp = np.logical_and(pred == 1, truth == 1)
                            fp = np.logical_and(pred == 1, truth == 0)
                            fn = np.logical_and(pred == 0, truth == 1)
                            tn = np.logical_and(pred == 0, truth == 0)
                    
                            #Draw TP, FP, FN
                            draw_bbox(img, props[np.where(tp)], color='green')
                            draw_bbox(img, props[np.where(fp)], color='red')
                            draw_bbox(img, props[np.where(fn)], color='blue')
                            draw_bbox(img, props[np.where(tn)], color='orange')
                            img.save(os.path.join(self.outputdir, imgname[0]))

                if (idx + 1) % 200 == 0:
                    print("End of testing %d samples" %(idx + 1))
       
        if len(self.roi_layers) > 0:
        
            for i in range(len(self.roi_layers)): # Calculation of Precision/Recall Confusion Matrix of ROIClass

                truth_ls = np.array(list(itertools.chain.from_iterable(truth_list[i])))
                pred_ls = np.array(list(itertools.chain.from_iterable(pred_list[i])))
                
                # Calculating Precision-Recall
                tp = np.sum(np.logical_and(truth_ls == 1, pred_ls == 1))
                tn = np.sum(np.logical_and(truth_ls == 0, pred_ls == 0))
                fp = np.sum(np.logical_and(truth_ls == 0, pred_ls == 1))
                fn = np.sum(np.logical_and(truth_ls == 1, pred_ls == 0))
            
                # For Foreground
                pr1 = tp / (tp + fp + eps)
                re1 = tp / (tp + fn + eps)
                f1score = 2 * pr1 * re1 / (pr1 + re1 + eps)

                # For Background
                pr0 = tn / (tn + fn + eps)
                re0 = tn / (tn + fp + eps)
                f0score = 2 * pr0 * re0 / (pr0 + re0 + eps)

                print("Class: %s Total Examples: %d Foreground: TPs: %d, FPs: %d | roi_precision: %.3f, roi_recall: %.3f, roi_fsocre: %.3f" % (self.roi_layers[i].class_size, sum(truth_ls == 1), tp, fp, pr1, re1, f1score))
                print("Class: %s Total Examples: %d Background: TNs: %d, FNs: %d | roi_precision: %.3f, roi_recall: %.3f, roi_fsocre: %.3f" % (self.roi_layers[i].class_size, sum(truth_ls == 0), tn, fn, pr0, re0, f0score))
    

# Testbench == SUCCESS
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', '-d',
        type=str, default='cfg/sketch.data', help='data definition file')
    parser.add_argument('--config', '-c',
        type=str, default='cfg/sketch.cfg', help='network configuration file')
    parser.add_argument('--weights', '-w',
        type=str, help='initial weights file')
    parser.add_argument('--weights_roi', '-wr',
        type=str, help='initial weights file roi')
    parser.add_argument('--reset', '-r',
        action="store_true", default=False, help='initialize the epoch and model seen value')

    print('Demo Command: python validator.py -c <path-to-cfg> -d <path-to-datafile> -w <path-to-detector-weights> -wr <path-to-branch-weights> -r <reset training flag>')

    FLAGS, _ = parser.parse_known_args()
    
    validator = YOLOv3ROICLassValidator(FLAGS)
    validator.validate()
