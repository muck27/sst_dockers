#!/usr/bin/python
# encoding: utf-8
from __future__ import division
import os
import random
import torch
import numpy as np
from torch.utils.data import Dataset
from PIL import Image
from utils import read_truths_args, read_truths
from image import *
from torchvision import datasets, transforms


def custom_collate(batch):

    data = torch.stack([item[0] for item in batch], 0)
    targets = torch.stack([item[1] for item in batch], 0)
    target_proposals = torch.stack([item[2] for item in batch], 0)
    data_proposals= torch.stack([item[3] for item in batch], 0)
    
    return data, targets, target_proposals, data_proposals

class listDataset(Dataset):
    def __init__(self, root, shape=None, shuffle=True, crop=False, jitter=0.3, hue=0.1, saturation=1.5, exposure=1.5, multiscale=True, transform=None, target_transform=None, train=False, seen=0, batch_size=64, num_workers=1):
       with open(root, 'r') as file:
           self.lines = file.readlines()

       if shuffle:
           random.shuffle(self.lines)

       self.nSamples  = len(self.lines)
       self.transform = transform
       self.target_transform = target_transform
       self.train = train
       self.shape = shape
       self.seen = seen
       self.batch_size = batch_size
       self.num_workers = num_workers
       self.crop = crop 
       self.jitter = jitter
       self.hue = hue
       self.saturation = saturation
       self.exposure = exposure
       self.multiscale = multiscale

    def __len__(self):
        return self.nSamples

    def get_different_scale(self):

        # if self.seen < 4000*self.batch_size:
        #     wh = 13*32                          # 416
        # elif self.seen < 8000*self.batch_size:
        #     wh = (random.randint(0,3) + 13)*32  # 416, 480
        # elif self.seen < 12000*self.batch_size:
        #     wh = (random.randint(0,5) + 12)*32  # 384, ..., 544
        # elif self.seen < 16000*self.batch_size:
        #     wh = (random.randint(0,7) + 11)*32  # 352, ..., 576
        # else: # self.seen < 20000*self.batch_size:
        wh = (random.randint(0, 9) + 10) * 32  # 320, ..., 608
        return (wh, wh)

    def __getitem__(self, index):
        
        assert index <= len(self), 'index range error'
        imgpath = self.lines[index].rstrip()

        if self.train:            
            if self.seen % (self.batch_size * 10) == 0 and self.multiscale: # Bugfix needed, multiscale training
                self.shape = self.get_different_scale()
                # print("Rescaled: %d | Index Value: %d Batch No: %d" % (self.shape[0], index, self.seen / (self.batch_size)))

            img, label, label_proposals, proposals = load_data_detection(imgpath, self.shape, self.crop, self.jitter, self.hue, self.saturation, self.exposure, (index % self.batch_size))
            
            label = torch.from_numpy(label)
            label_proposals = torch.from_numpy(label_proposals)
            proposals = torch.from_numpy(proposals)
        else:
            imgname = os.path.basename(imgpath)
            img = Image.open(imgpath).convert('RGB')

            if self.shape:
                img, org_w, org_h = letterbox_image(img, self.shape[0], self.shape[1]), img.width, img.height
            
            labpath = imgpath.replace('images', 'labels').replace('JPEGImages', 'labels').replace('.jpg', '.txt').replace('.png','.txt')
            
            label = torch.zeros(50*5)
            try:
                tmp, tmp_prop, prop = read_truths_args(labpath, 2.0 / img.width)
                correct_proposal_boxes(prop[:,1:], org_w, org_h, self.shape[0], self.shape[1])
                prop = convert_from_YOLO(prop, self.shape[0], self.shape[1])

                tmp = torch.from_numpy(tmp.astype(np.float32))
                tmp_prop = torch.from_numpy(tmp_prop.astype(np.float32))
                prop = torch.from_numpy(prop.astype(np.float32))
                
            except Exception:
                print("Somethings are not good in this world..!!")
                tmp = torch.zeros(1, 5)
                tmp_prop = torch.zeros(1, 5)

            tmp = tmp.view(-1)
            tsz = tmp.numel()

            if tsz > 50*5:
                label = tmp[0:50*5]
            elif tsz > 0:
                label[0:tsz] = tmp

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            label = self.target_transform(label)
            
        self.seen = self.seen + 1

        if self.train:
            return (img, label, label_proposals, proposals)
        else:
            return (img, label, org_w, org_h, imgname, tmp_prop, prop)

#TestBench SUCCESS
if __name__ == '__main__':
    flag = True
    kwargs={}
    init_height, init_width = 608, 608

    test_loader = torch.utils.data.DataLoader(
        listDataset("coco_validate.txt", shape=(608, 608),
                       shuffle=False,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                       ]), train=False),
        batch_size=1, shuffle=False, **kwargs)
        
    for (data, target, org_w, org_h, imgname, target_prop, prop) in test_loader:
        print(prop)
