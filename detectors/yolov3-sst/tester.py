'''
Author: Shreejal Trivedi

Date: 6th October 2020

Description: Inference code of the YOLOv3 + ROIClass Branch. This file contains the inference class definition named YOLOv3ROIClass(yolov3_roiclass.py is deprecated).
             
'''

from __future__ import print_function
from __future__ import division
import torch
from torchvision import transforms
import os
import numpy as np
from utils import get_all_boxes, nms, read_data_cfg, logging
from darknet_v2 import Darknet
from image import correct_yolo_boxes, correct_proposal_boxes, letterbox_image, draw_bbox
from PIL import Image, ImageDraw, ImageFont
import copy
from bgsrunner import BGSRunner
import cv2
import torch.nn.functional as F

class YOLOv3ROIClassTester():

    def __init__(self, datafile, cfgfile, weightfile, weightfile_roiclass=None, use_cuda=None, 
                    conf_thresh=0.05, min_prop_thresh=0.05, nms_thresh=0.45, max_prop_thresh=0.5, seed=2222, min_text_conf=0.3, max_text_conf=0.5, mapping=False, USE_CLASS=False, DEBUG=False, INTEGRATE_BGS=False):

        # Class Initializations

        self.datafile = datafile
        self.cfgfile = cfgfile
        self.weightfile = weightfile
        self.weightfile_roiclass = weightfile_roiclass
        self.use_cuda = torch.cuda.is_available() and (True if use_cuda is None else use_cuda)
        self.device = torch.device('cuda' if self.use_cuda else 'cpu')
        self.seed = seed
        self.transform = transforms.Compose([transforms.ToTensor()])
        self.use_class = USE_CLASS
        self.debug = DEBUG
        self.INTEGRATE_BGS = INTEGRATE_BGS

        # Inference value initializations

        self.conf_thresh = conf_thresh
        self.nms_thresh = nms_thresh
        self.min_prop_thresh = min_prop_thresh
        self.max_prop_thresh = max_prop_thresh
        self.min_text_conf = min_text_conf
        self.max_text_conf = max_text_conf
        self.mapping = mapping
        self.indices = [6, 4, 0, 1, 2, 3, 5]

        # Intialize the model

        self.init_detector()

        # Initialize BGS model

        if self.INTEGRATE_BGS:
            self.runner = BGSRunner(width=self.model.width, height=self.model.height, bgs_algorithm='GSOC')
            print('BGS INTEGRATION is ON...Please use video frames for a good BGS motion update. Random frames not recommended..!')

        # Make some folders for storing results and text outputs and BGS masks if Flag is ON

        if not os.path.exists('detection_results'):
            os.mkdir('detection_results')
        if not os.path.exists('detection_texts'):
            os.mkdir('detection_texts')

        if self.INTEGRATE_BGS: 
            if not os.path.exists('bgs_masks/frames'):
                os.makedirs('bgs_masks/frames')
            if not os.path.exists('bgs_masks/fmask'):
                os.makedirs('bgs_masks/fmask')
            if not os.path.exists('bgs_masks/dmask'):
                os.makedirs('bgs_masks/dmask')

    
    def init_detector(self):

        # Parse Datafile

        data_options = read_data_cfg(self.datafile)
        gpus = data_options['gpus']
        ngpus = len(gpus.split(','))

        # Initialize Detector

        self.model = Darknet(self.cfgfile, self.use_cuda)
        self.model.load_weights(self.weightfile)
        if self.weightfile_roiclass is not None and len(self.model.roi_layers) > 0 and self.use_class:
            self.model.load_weights_roi(self.weightfile_roiclass)

        # GPU Configurations if any

        torch.manual_seed(self.seed)

        if self.use_cuda:
            os.environ['CUDA_VISIBLE_DEVICES'] = gpus
            torch.cuda.manual_seed(self.seed)

        if self.use_cuda:
            if ngpus > 1:
                self.model = torch.nn.DataParallel(self.model)
                self.model = self.model.module
        
        # Transfer the model onto GPU/CPU device

        self.model = self.model.to(self.device)
        self.model.eval()


    def preprocess_image(self, frame):

        # Convert CV2 to PIL Image 

        frame = Image.fromarray(frame)
        
        (frame_width, frame_height) = frame.size

        frame = letterbox_image(frame, self.model.width, self.model.height)

        # Apply transforms

        frame = self.transform(frame)
        frame = frame.to(self.device)
        frame = frame.unsqueeze(0)

        return frame, frame_width, frame_height

    def convert_PIL_to_CV(self, img):

        opencv_img = np.array(img)[:, :, ::-1]
        return opencv_img
    
    def convert_yolo_to_proposals(self, boxes, width, height):

        if boxes.shape[0] > 0:

            xc, yc, w, h, conf, cls, cls_idx = np.split(boxes, 7, axis=1)
            
            xmin = (xc * width) - (w * width) / 2.        
            xmax = xmin + (w * width)
            ymin = (yc * height) - (h * height) / 2.    
            ymax = ymin + (h * height)

            return np.hstack((np.clip(xmin, 0, width), np.clip(ymin, 0, height), np.clip(xmax, 0, width), np.clip(ymax, 0, height), conf, cls, cls_idx))
        else:
            return np.array([])

    def get_roiclass_format(self, proposals, idxs):

        if proposals.shape[0] > 0:

            prop = proposals[idxs]
            prop = prop[:, :4]
            prop = np.hstack((np.zeros(shape=(prop.shape[0], 1)), prop))
            prop = torch.from_numpy(prop)
            
            return prop

        else:
            return torch.from_numpy(np.array([]))
    
    def make_bgs_boxes(self, boxes):

        if boxes.shape[0] > 0:

            conf = np.empty(shape=(boxes.shape[0], 1))
            conf.fill(0.5)
            cls_idx = np.ones(shape=(boxes.shape[0], 1))
            cls = np.zeros(shape=(boxes.shape[0], 1))
            return np.hstack((boxes[:, 1:], conf, cls_idx, cls))
        else:
            return np.array([])
        

    def convert_from_proposals(self, boxes, im_w, im_h, net_w, net_h):

        if boxes.shape[0] > 0:
            x1, y1, x2, y2, conf, cls_idx, cls = np.split(boxes, 7, axis=1)

            xc = ((x1 + x2) / 2.) / net_w
            yc = ((y1 + y2) / 2.) / net_h
            w = (x2 - x1) / net_w
            h = (y2 - y1) / net_h

            bboxes = np.hstack((xc, yc, w, h))

            im_w, im_h = float(im_w), float(im_h)

            net_w, net_h = float(net_w), float(net_h)

            if net_w / im_w < net_h / im_h:
                new_w = net_w
                new_h = (im_h * net_w) / im_w
            else:
                new_w = (im_w * net_h) / im_h
                new_h = net_h

            xo, xs = (net_w - new_w) / (2 * net_w), new_w / net_w
            yo, ys = (net_h - new_h) / (2 * net_h), new_h / net_h

            bboxes[:, 0] = (bboxes[:, 0] - xo) / xs
            bboxes[:, 1] = (bboxes[:, 1] - yo) / ys
            bboxes[:, 2] = bboxes[:, 2] / xs
            bboxes[:, 3] = bboxes[:, 3] / ys
        
            bboxes = np.hstack((bboxes, conf, cls_idx, cls))
            bboxes = self.convert_yolo_to_proposals(bboxes, im_w, im_h)

            return bboxes
        else:
            return np.array([])
        
    def dump_detection_results(self, boxes, frame, imgname, prediction_list_detector=None, conf_list_detector=None, proposal_idxs=None, prediction_list_bgs=None, boxes_bgs=None):
        
        for i, bb in enumerate(boxes): # Detection Boxes
            
            color = 'green'
            if prediction_list_detector is not None:
                if i in prediction_list_detector:
                    color = 'blue'
                elif bb[4] < self.max_prop_thresh: 
                    color = 'red'

            draw_bbox(frame, bb[:4].reshape(1, -1), color=color)

            draw = ImageDraw.Draw(frame)
            draw.text((bb[0], bb[1]), str(round(bb[4], 2)), fill="white", align="center")                

        if boxes_bgs is not None:

            for i, bb in enumerate(boxes_bgs):

                color = 'cyan' if i in prediction_list_bgs else 'orange'
                draw_bbox(frame, bb[:4].reshape(1, -1), color=color)

        frame.save(os.path.join('detection_results', imgname))
    
    def dump_detection_texts(self, boxes, imgname, prediction_list_detector=None, prediction_list_bgs=None, boxes_bgs=None):

        # Detector texts
        if boxes.shape[0] > 0:
            texts_detector = boxes[:, self.indices][np.where(boxes[:, 4] > self.max_prop_thresh)[0]]
            if texts_detector.shape[0] > 0:
                texts_detector = np.delete(texts_detector, 6, 1)
            texts_detector = texts_detector.reshape(texts_detector.shape[0], 6)

            # Classifier texts
            
            texts_classifier = boxes[:, self.indices][prediction_list_detector.tolist()]
            if texts_classifier.shape[0] > 0:
                texts_classifier = np.delete(texts_classifier, 6, 1) 
            texts_classifier = texts_classifier.reshape(texts_classifier.shape[0], 6)
            # BGS texts
            
            if prediction_list_bgs is not None: 
                if boxes_bgs.shape[0] > 0:
                    texts_bgs = boxes_bgs[:, self.indices][prediction_list_bgs.tolist()]
                    texts_bgs = np.delete(texts_bgs, 6, 1)
                else:
                    texts_bgs = np.array([])

                texts_bgs = texts_bgs.reshape(texts_bgs.shape[0], 6)
            
            # Preprocess Classifier texts. Mapping the confidence values
            if self.mapping:
                if texts_classifier.shape[0] > 0:            
                    idxs = np.where((texts_classifier[:, 1] > self.min_text_conf) & (texts_classifier[:, 1] <= self.max_text_conf))[0]
                    texts_classifier[idxs, 1] = np.random.uniform(0.5, 0.6)

            texts_total = np.vstack((texts_detector, texts_classifier))

            if prediction_list_bgs is not None:
                texts_total = np.vstack((texts_total, texts_bgs))
        else:
            texts_total = np.array([])
        
        np.savetxt(os.path.join('detection_texts', imgname.replace('.jpg', '.txt')), texts_total)
    
    def run_detector(self, frame, imgname):

        ''' 
        Main Function: Takes Frame and image name as an input and save the results of the Detector/Detector + Classifier output in the folder
                        'detection_results' and 'detection_texts'
                    
        '''
        #frame = frame[120:frame.shape[0], 320:frame.shape[1]]
        frame_orig = copy.deepcopy(frame)
        frame_orig = Image.fromarray(frame_orig)
        frame, frame_width, frame_height = self.preprocess_image(frame)
        shape = (self.model.width, self.model.height)

        # Forward Pass: If USE_CLASS is true then only do forward pass in classifer branch

        if self.use_class and len(self.model.roi_layers) > 0:
            output, output_fm = self.model(frame)
        else:
            output = self.model(frame)

        if self.INTEGRATE_BGS: # Apply BGS algorithm to the present frame if flag is ON
            img_bgs = self.convert_PIL_to_CV(transforms.ToPILImage()(frame[0].cpu()))
            self.runner.apply(img_bgs)
            if self.debug:
                cv2.imwrite(os.path.join('bgs_masks/fmask', imgname), self.runner.fgMask)
        
        all_boxes = get_all_boxes(output, shape, self.conf_thresh, self.model.num_classes, use_cuda=self.use_cuda, use_class=self.use_class)

        for k in range(len(all_boxes)):

            boxes = all_boxes[k]
            correct_yolo_boxes(boxes, frame_width, frame_height, self.model.width, self.model.height)
            correct_proposal_boxes(boxes, frame_width, frame_height, self.model.width, self.model.height)
            
            boxes = np.array(nms(boxes, self.nms_thresh))
            boxes = self.convert_yolo_to_proposals(boxes, self.model.width, self.model.height) 

            # This is the classifier branch.
             
            if self.use_class and len(self.model.roi_layers) > 0:
                
                prediction_list_detector = list()
                
                if self.INTEGRATE_BGS:
                    prediction_list_bgs = list()

                proposal_idxs = np.where((boxes[:, 4] >= self.min_prop_thresh) & (boxes[:, 4] < self.max_prop_thresh))[0] if boxes.shape[0] > 0 else np.array([])  
                proposals = self.get_roiclass_format(boxes, proposal_idxs)
                
                # Update Detection Mask by passing proposals to the runner
                if self.INTEGRATE_BGS: 

                    bgs_contours = self.runner.getCountours()
                    self.runner.updateDetMask(proposals.cpu().numpy())
                    boxes_detections = proposals[:, 1:].cpu().numpy() if proposals.shape[0] > 0 else np.array([])
                    boxes_final_bgs = self.runner.checkUnwantedBoxes(boxes_detections, bgs_contours)
                    
                 # Forward pass in roiclass branch

                for j, l in enumerate(self.model.roi_layers):
                    
                    proposals_filters, idx_filters = l.candidate_proposals_validation(proposals) if proposals.shape[0] > 0 else (torch.from_numpy(np.array([])).long().to(self.device), np.array([]))
                    
                    proposals_filters_total = proposals_filters
                    if self.INTEGRATE_BGS:
                        boxes_final_bgs = torch.Tensor(boxes_final_bgs).double().to(self.device) # Convert the boxes to Tensor

                        if proposals_filters.size(0) > 0:
                            proposals_filters_total = torch.cat([proposals_filters, boxes_final_bgs], dim=0) if boxes_final_bgs.size(0) > 0 else proposals_filters
                        else:
                            proposals_filters_total = boxes_final_bgs

                    if proposals_filters_total.size(0) > 0:
                        out = l(output_fm[j]['x'], proposals_filters_total)
                        
                        pred_total = np.argmax(out.data.cpu().numpy(), axis=1)
                        conf_total = np.max(F.softmax(out, dim=1).data.cpu().numpy(), axis=1)
                        
                        pred_detector = pred_total[:proposals_filters.size(0)] if proposals_filters.size(0) > 0 else []
                        conf_detector = conf_total[:proposals_filters.size(0)] if proposals_filters.size(0) > 0 else []
                    else:
                        pred_total = []
                        pred_detector = []
                        conf_detector = []
                    
                    if self.INTEGRATE_BGS:
                        boxes_final_bgs = boxes_final_bgs.detach().cpu().numpy()
                        pred_bgs = pred_total[-boxes_final_bgs.shape[0]:] if boxes_final_bgs.shape[0] > 0 else []
                        self.runner.updateDetMask(boxes_final_bgs[np.where(pred_bgs == 1)], flag=False)

                        if self.debug:
        
                            cv2.imwrite(os.path.join('bgs_masks/dmask', imgname), self.runner.detMask)
                            cv2.imwrite(os.path.join('bgs_masks/frames', imgname), img_bgs)

                        self.runner.update(img_bgs)
                    else:
                        pred_bgs = []
                    
                    if len(pred_detector) > 0:
                        prediction_list_detector.append(list(proposal_idxs[idx_filters[np.where(pred_detector == 1)]]))
                    
                    if self.INTEGRATE_BGS:
                        prediction_list_bgs.append(list(np.where(pred_bgs == 1)[0]))

                prediction_list_detector = np.array(sum(prediction_list_detector, []))

                if self.INTEGRATE_BGS:
                    prediction_list_bgs = np.array(sum(prediction_list_bgs, []))
                    boxes_final_bgs = self.make_bgs_boxes(boxes_final_bgs)

            # Final Detection and BGS(If flag is on) conversion
            boxes = self.convert_from_proposals(boxes, frame_width, frame_height, self.model.width, self.model.height)
            tmp ={}
            tmp['boxes'] = boxes.tolist()
            tmp['img'] = imgname

            if self.INTEGRATE_BGS:
                boxes_final_bgs = self.convert_from_proposals(boxes_final_bgs, frame_width, frame_height, self.model.width, self.model.height)

        if self.debug:
            if self.use_class and len(self.model.roi_layers) > 0:
                if self.INTEGRATE_BGS:
                    self.dump_detection_results(boxes, frame_orig, imgname, prediction_list_detector=prediction_list_detector, conf_list_detector=conf_detector, proposal_idxs=proposal_idxs, prediction_list_bgs=prediction_list_bgs, boxes_bgs=boxes_final_bgs)
                else:
                    self.dump_detection_results(boxes, frame_orig, imgname, prediction_list_detector=prediction_list_detector, conf_list_detector=conf_detector, proposal_idxs=proposal_idxs)
            else:
                self.dump_detection_results(boxes, frame_orig, imgname)

        if self.use_class and len(self.model.roi_layers) > 0:
            if self.INTEGRATE_BGS:
                self.dump_detection_texts(boxes, imgname, prediction_list_detector=prediction_list_detector, prediction_list_bgs=prediction_list_bgs, boxes_bgs=boxes_final_bgs)
            else:
                self.dump_detection_texts(boxes, imgname, prediction_list_detector=prediction_list_detector)

        tmp2 = {}
        tmp2['value']= tmp
        return (tmp2)

