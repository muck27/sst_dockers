from __future__ import division
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from cfg import *
import numpy as np
from yolo_layer import YoloLayer
from torchvision.ops import RoIPool, RoIAlign
from torchvision import transforms
import dataset
import time

#This is the darknet model generation file with the support of the CBAM/BAM/Modified SAM Functionality

class BasicConv(nn.Module):
      def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1, groups=1, relu=True, bn=True, bias=False):
        super(BasicConv, self).__init__()
        self.out_channels = out_planes
        self.conv = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, groups=groups, bias=bias)
        self.bn = nn.BatchNorm2d(out_planes, eps=1e-5, momentum=0.01, affine=True) if bn else None
        self.relu = nn.ReLU() if relu else None

      def forward(self, x):
          x = self.conv(x)
          if self.bn is not None:
             x = self.bn(x)
          if self.relu is not None:
             x = self.relu(x)
          return x

class Flatten(nn.Module):
    def forward(self, x):
        return x.view(x.size(0), -1)

class ChannelGate(nn.Module):
    def __init__(self, gate_channels, reduction_ratio=16, pool_types=['avg', 'max']):
        super(ChannelGate, self).__init__()
        self.gate_channels = gate_channels
        self.mlp = nn.Sequential(
            Flatten(),
            nn.Linear(gate_channels, gate_channels // reduction_ratio),
            nn.ReLU(),
            nn.Linear(gate_channels // reduction_ratio, gate_channels)
            )
        self.pool_types = pool_types
    def forward(self, x):
        channel_att_sum = None
        for pool_type in self.pool_types:
            if pool_type=='avg':
                avg_pool = F.avg_pool2d( x, (x.size(2), x.size(3)), stride=(x.size(2), x.size(3)))
                channel_att_raw = self.mlp( avg_pool )
            elif pool_type=='max':
                max_pool = F.max_pool2d( x, (x.size(2), x.size(3)), stride=(x.size(2), x.size(3)))
                channel_att_raw = self.mlp( max_pool)

            if channel_att_sum is None:
                channel_att_sum = channel_att_raw
            else:
                channel_att_sum = channel_att_sum + channel_att_raw

        scale = torch.sigmoid(channel_att_sum).unsqueeze(2).unsqueeze(3).expand_as(x)
        return x * scale

class ChannelPool(nn.Module):
    def forward(self, x):
        return torch.cat( (torch.max(x,1)[0].unsqueeze(1), torch.mean(x,1).unsqueeze(1)), dim=1 )


class SpatialGate(nn.Module):#Default SAM
    def __init__(self):
        super(SpatialGate, self).__init__()
        kernel_size = 7
        self.compress = ChannelPool()
        self.spatial = BasicConv(2, 1, kernel_size, stride=1, padding=(kernel_size-1) // 2, relu=False)
    def forward(self, x):
        x_compress = self.compress(x)
        x_out = self.spatial(x_compress)
        scale = torch.sigmoid(x_out) # broadcasting
        return x * scale

class CBAM(nn.Module):
    def __init__(self, gate_channels, reduction_ratio=16, pool_types=['avg', 'max'], no_spatial=False):
        super(CBAM, self).__init__()
        self.ChannelGate = ChannelGate(gate_channels, reduction_ratio, pool_types)
        self.no_spatial=no_spatial
        if not no_spatial:
            self.SpatialGate = SpatialGate()
    def forward(self, x):
        x_out = self.ChannelGate(x)
        if not self.no_spatial:
            x_out = self.SpatialGate(x_out)
        return x_out


class SAM(nn.Module): #SAM Block YOLOv4 Version
    def __init__(self, in_filters, kernel_size=7):
        super(SAM, self).__init__()
        self.kernel_size = kernel_size
        self.in_filters = in_filters
        padding = 1 if self.kernel_size == 3 else (self.kernel_size - 1) // 2
        self.spatial = BasicConv(self.in_filters, 1, kernel_size=self.kernel_size, stride=1, padding=padding, relu=False)

    def forward(self, x):
        x_out = self.spatial(x)
        scale = torch.sigmoid(x_out)
        return x * scale

class ROIFPN(nn.Module):

    def __init__(self, filters8=64, filters16=128, filters32=256, channel_reducer=64, bottleneck_ratio=2):
        super(ROIFPN, self).__init__()

        # Some initializations
        self.filters8 = filters8
        self.filters16 = filters16
        self.filters32 = filters32

        self.filters_stage1 = channel_reducer
        self.filters_stage2 = self.filters16 + self.filters_stage1
        self.filters_stage3 = self.filters_stage2 + self.filters8
        self.filters_stage2_bottleneck = self.filters_stage2 // bottleneck_ratio
        self.filters_stage3_bottleneck = self.filters_stage3 // bottleneck_ratio

        # Initialization of FPN ROICLASS Network

        # Stage 2 ---> Stride 16
        
        self.cr1 = nn.Conv2d(self.filters32, self.filters_stage1, kernel_size=1, stride=1, padding=0, bias=False)
        self.up1 = nn.Upsample(scale_factor=2, mode='bilinear')

        self.conv1 = nn.Conv2d(self.filters_stage2, self.filters_stage2_bottleneck, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn1 = nn.BatchNorm2d(self.filters_stage2_bottleneck)

        self.conv2 = nn.Conv2d(self.filters_stage2_bottleneck, self.filters_stage2, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(self.filters_stage2)

        # Stage 3 ---> Stride 8 

        self.up2 = nn.Upsample(scale_factor=2, mode='bilinear')

        self.conv3 = nn.Conv2d(self.filters_stage3, self.filters_stage3_bottleneck, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn3 = nn.BatchNorm2d(self.filters_stage3_bottleneck)

        self.conv4 = nn.Conv2d(self.filters_stage3_bottleneck, self.filters_stage3, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn4 = nn.BatchNorm2d(self.filters_stage3)


    def forward(self, x, xlist):

        x8, x16 = xlist[0], xlist[1]
        # Channel Reduction and Upsampling of Stage 1
       
        out = self.cr1(x)
        out = self.up1(out)

        # Channel concatenation of stride=32 and stride=16

        out = torch.cat((out, x16), 1)

        # Convolutional Layer 1
       
        out = self.conv1(out)
        out = self.bn1(out)
        out = F.relu(out)

        # Convolutional Layer 2
       
        out = self.conv2(out)
        out = self.bn2(out) 
        out = F.relu(out)

        # Upsampling of Stage 2
        out = self.up2(out)

        # Channel concatenation of stride=16 and stride=8
        out = torch.cat((out, x8), 1)

        # Convolutional Layer 3

        out = self.conv3(out)
        out = self.bn3(out)
        out = F.relu(out)

        # Convolutional Layer 4

        out = self.conv4(out)
        out = self.bn4(out)
        out = F.relu(out)

        return out

class ROIClass(nn.Module):
    def __init__(self, scaled_window, spatial_scale, out_filters, in_filters, class_size='all', total_proposals=2, num_classes=2, use_cuda=None):
        super(ROIClass, self).__init__()
        
        use_cuda = torch.cuda.is_available() and (True if use_cuda is None else use_cuda)
        self.device = torch.device("cuda" if use_cuda else "cpu")

        self. scaled_window = scaled_window
        self.spatial_scale = spatial_scale
        self.total_proposals = total_proposals
        self.out_filters = out_filters
        self.in_filters = in_filters
        self.num_classes = num_classes
        self.seen = 0       
        self.class_size = class_size
        self.area_small = 32 * 32
        self.area_medium = 96 * 96

        # FPN Network Initialization
        self.roifpn = ROIFPN()

        '''
        #ROI Pooling layer for fixating the size of the proposals: 
        # Input --> Feature Map[batch_size X in_channels X in_width X in_height], ROIS[total_proposals X 5]
        
        # Output --> [total_proposals X in_channels X scaled_window X scaled_window]
        '''

        self.roi_pooling = RoIPool(output_size=(self.scaled_window, self.scaled_window), spatial_scale=self.spatial_scale)
        
        #Convolution Layer 1
        self.conv1 = nn.Conv2d(self.in_filters, self.out_filters, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(self.out_filters)
        
        #Convolution Layer 2
        self.conv2 = nn.Conv2d(self.out_filters, self.out_filters, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(self.out_filters)

 
        #SAM Layer 1
        #self.sam1 = SAM(self.out_filters)
        
        #Average Pool Layer 
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        #Out Layer: BG/FG
        self.linear = nn.Linear(self.out_filters, num_classes)

    # def return_fm(self, fm):

    #     return {'x': fm}
    
    def return_fm(self, fm, list_fm):

        fm_final = self.roifpn(fm, list_fm)
        return {'x': fm_final}

    def get_area(self, proposals):

        area = [(box[2] - box[0]) * (box[3] - box[1]) for box in proposals]
        return np.array(area)

    def candidate_proposals(self, proposals, label_proposals):

        '''
        This function is used only while training.
        
        '''
        area_proposals = self.get_area(proposals[:, 1:])        
        
        #Classifier Size Independent Training

        if self.class_size == 'small':
            
            idxs = np.where(area_proposals < self.area_small)
            return proposals[idxs].to(self.device), label_proposals[idxs].to(self.device)
        
        elif self.class_size == 'medium':

            idxs = np.where((area_proposals >= self.area_small) & (area_proposals < self.area_medium))
            return proposals[idxs].to(self.device), label_proposals[idxs].to(self.device)
        
        elif self.class_size == 'large':

            idxs = np.where(area_proposals >= self.area_medium)
            return proposals[idxs].to(self.device), label_proposals[idxs].to(self.device)
        
        else:

            return proposals.to(self.device), label_proposals.to(self.device) 

    def candidate_proposals_validation(self, proposals):

        area_proposals = self.get_area(proposals[:, 1:])

        #Classifier Size Independent Training
        if self.class_size == 'small':

            idxs = np.where(area_proposals < self.area_small)
            return proposals[idxs].to(self.device), idxs[0]

        elif self.class_size == 'medium':
            idxs = np.where((area_proposals >= self.area_small) & (area_proposals < self.area_medium))
            return proposals[idxs].to(self.device), idxs[0]

        elif self.class_size == 'large':
            idxs = np.where(area_proposals >= self.area_medium)
            return proposals[idxs].to(self.device), idxs[0]

        else:
            return proposals.to(self.device), np.arange(0, proposals.shape[0])

    def forward(self, feature_map, proposals):

        '''
        By default SAM module is integrated with this forward pass of ROIClass, but are commented to make it default.
        If you want to use the SAM module, uncomment 'residual = out', SAM Layer 1.
        
        IMPORTANT: YOLOv3 Person Pruned Model is trained without SAM Architecture.  
        '''
        #RoI Pooling Layer
        out = self.roi_pooling(feature_map, proposals.float())        

        #Convolutional Layer 1
        out = self.conv1(out)
        out = self.bn1(out)
        out = F.relu(out)
       
        # residual = out  
        #Convolutional Layer 2
        out = self.conv2(out)
        out = self.bn2(out)
        out = F.relu(out)
  
        #SAM Layer 1
        # out = self.sam1(out)
        # out += residual
        # out = F.relu(out)

        #Average Pooling Layer
        out = self.avgpool(out)

        #Flatten for FC
        out = torch.flatten(out, 1)

        #Classifier
        out = self.linear(out)

        return out

class Upsample(nn.Module):
    def __init__(self, stride=2):
        super(Upsample, self).__init__()
        self.stride = stride
    def forward(self, x):
        stride = self.stride
        assert(x.data.dim() == 4)
        B = x.data.size(0)
        C = x.data.size(1)
        H = x.data.size(2)
        W = x.data.size(3)
        ws = stride
        hs = stride
        x = x.view(B, C, H, 1, W, 1).expand(B, C, H, hs, W, ws).contiguous().view(B, C, H*hs, W*ws)
        return x

# for route and shortcut
class EmptyModule(nn.Module):
    def __init__(self):
        super(EmptyModule, self).__init__()

    def forward(self, x):
        return x

# support route shortcut and reorg

class Darknet(nn.Module):

    def getROILayers(self):
        roi_layers = []
        for m in self.models:
            if isinstance(m, ROIClass):
                roi_layers.append(m)
        return roi_layers

    def sample_proposals(self, proposals, label_proposals):

        proposals = proposals.cpu()
        label_proposals = label_proposals.cpu()

        proposals = np.delete(proposals, np.where(np.all(np.isclose(proposals, 0), axis=1)), axis=0)
        label_proposals = np.delete(label_proposals, np.where(np.all(np.isclose(label_proposals, 0), axis=1)), axis=0)[:,0].long()
     
        #Uncomment these three lines for random shuffle in minibatch of classifier
        #shuffle_idxs = np.random.shuffle(np.arange(0, proposals.shape[0]))
        #proposals = proposals[shuffle_idxs]
        #label_proposals = label_proposals[shuffle_idxs]        
        
        return proposals.to(self.device), label_proposals.to(self.device)

    def getLossLayers(self):
        loss_layers = []
        for m in self.models:
            if isinstance(m, YoloLayer):
                loss_layers.append(m)
        return loss_layers

    def __init__(self, cfgfile, use_cuda=False):
        super(Darknet, self).__init__()
        self.use_cuda = use_cuda
        self.device = torch.device("cuda" if use_cuda else "cpu")
        self.use_cuda= use_cuda
        self.blocks = parse_cfg(cfgfile)
        self.models = self.create_network(self.blocks)
        self.loss_layers = self.getLossLayers()
        self.roi_layers = self.getROILayers()
 
        if len(self.loss_layers) > 0:
            last = len(self.loss_layers)-1
            self.anchors = self.loss_layers[last].anchors
            self.num_anchors = self.loss_layers[last].num_anchors
            self.anchor_step = self.loss_layers[last].anchor_step
            self.num_classes = self.loss_layers[last].num_classes

        # default format : major=0, minor=1
        self.header = torch.IntTensor([0,1,0,0])
        self.seen = 0

    def forward(self, x):
        ind = -2
        
        roi_fm = list()
        outputs = dict()
        out_boxes = dict()
        out_fm = dict()
        outno = 0
        outroi=0
        for block in self.blocks:
            ind = ind + 1

            if block['type'] == 'net':
                continue
            elif block['type'] in ['convolutional', 'maxpool', 'reorg', 'upsample', 'avgpool', 'softmax', 'connected']:
                x = self.models[ind](x)
                outputs[ind] = x
            elif block['type'] == 'roiclass':
                fm = self.models[ind].return_fm(x, roi_fm)
                #fm = self.models[ind].return_fm(x)
                out_fm[outroi] = fm
                outroi +=1
                outputs[ind] = None

            elif block['type'] == 'route':
                layers = block['layers'].split(',')
                layers = [int(i) if int(i) > 0 else int(i)+ind for i in layers]
                if len(layers) == 1:
                    x = outputs[layers[0]]
                elif len(layers) == 2:
                    x1 = outputs[layers[0]]
                    x2 = outputs[layers[1]]
                    x = torch.cat((x1,x2),1)
                outputs[ind] = x

            elif block['type'] == 'shortcut':
                from_layer = int(block['from'])
                activation = block['activation']
                from_layer = from_layer if from_layer > 0 else from_layer + ind
                x1 = outputs[from_layer]
                x2 = outputs[ind-1]
                x  = x1 + x2
                if activation == 'leaky':
                    x = F.leaky_relu(x, 0.1, inplace=True)
                elif activation == 'relu':
                    x = F.relu(x, inplace=True)

                if ind in self.include_roi:
                    roi_fm.append(x)
                    
                outputs[ind] = x

            elif block['type'] in [ 'yolo']:
                boxes = self.models[ind].get_mask_boxes(x)
                out_boxes[outno]= boxes
                outno += 1
                outputs[ind] = None
            elif block['type'] == 'cost':
                continue
            else:
                print('unknown type %s' % (block['type']))
        
        if outno == 0:
           return x
        else:
            if outroi == 0:
               return out_boxes
            else:
               return (out_boxes, out_fm)
        

    def print_network(self):
        print_cfg(self.blocks)

    def create_network(self, blocks):
        models = nn.ModuleList()
        prev_filters = 3
        out_filters =[]
        prev_stride = 1
        out_strides = []
        conv_id = 0
        self.roi_index = []
        self.loss_layers = []
        self.roi_fm = []
        ind = -2
        for block in blocks:
            ind += 1

            if block['type'] == 'net':
                prev_filters = int(block['channels'])
                self.width = int(block['width'])
                self.height = int(block['height'])
                self.include_roi = block['include_roi'].split(', ')
                self.include_roi = list(map(int, self.include_roi)) 
                print("ROI INCLUDE INDICES: ", self.include_roi)
                prev_width = self.width
                prev_height = self.height
                continue

            elif block['type'] == 'convolutional':
                conv_id = conv_id + 1
                batch_normalize = int(block['batch_normalize'])
                filters = int(block['filters'])
                kernel_size = int(block['size'])
                stride = int(block['stride'])
                is_pad = int(block['pad'])
                pad = (kernel_size-1)//2 if is_pad else 0
                activation = block['activation']

                width = (prev_width + 2*pad - kernel_size)//stride + 1
                height = (prev_height + 2*pad - kernel_size)//stride + 1
                model = nn.Sequential()
                if batch_normalize:
                    model.add_module('conv{0}'.format(conv_id), nn.Conv2d(prev_filters, filters, kernel_size, stride, pad, bias=False))
                    model.add_module('bn{0}'.format(conv_id), nn.BatchNorm2d(filters))
                else:
                    model.add_module('conv{0}'.format(conv_id), nn.Conv2d(prev_filters, filters, kernel_size, stride, pad))
                if activation == 'leaky':
                    model.add_module('leaky{0}'.format(conv_id), nn.LeakyReLU(0.1, inplace=True))
                elif activation == 'relu':
                    model.add_module('relu{0}'.format(conv_id), nn.ReLU(inplace=True))
                prev_filters = filters
                out_filters.append(prev_filters)
                prev_width = width
                prev_height = height
                prev_stride = stride * prev_stride
                out_strides.append(prev_stride)                
                models.append(model)

            elif block['type'] == 'roiclass':
                self.roi_index.append(ind)
                class_size = str(block['class_size'])
                scaled_window = int(block['scaled_window'])
                filters_out = int(block['out_filters'])
                filters_in =int(block['in_filters'])
                spatial_scale = float(block['spatial_scale'])
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                assert(prev_width == prev_height)                 
                model = ROIClass(scaled_window, spatial_scale, filters_out, filters_in, class_size)
                models.append(model)

            elif block['type'] == 'upsample':
                stride = int(block['stride'])
                out_filters.append(prev_filters)
                prev_stride = prev_stride / stride
                out_strides.append(prev_stride)                
                models.append(Upsample(stride))

            elif block['type'] == 'route':
                layers = block['layers'].split(',')
                ind = len(models)
                layers = [int(i) if int(i) > 0 else int(i)+ind for i in layers]
                if len(layers) == 1:
                    prev_filters = out_filters[layers[0]]
                    prev_stride = out_strides[layers[0]]
                elif len(layers) == 2:
                    assert(layers[0] == ind - 1)
                    prev_filters = out_filters[layers[0]] + out_filters[layers[1]]
                    prev_stride = out_strides[layers[0]]
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                models.append(EmptyModule())

            elif block['type'] == 'shortcut':
                ind = len(models)
                prev_filters = out_filters[ind-1]
                out_filters.append(prev_filters)
                prev_stride = out_strides[ind-1]
                out_strides.append(prev_stride)
                models.append(EmptyModule())

            elif block['type'] == 'yolo':
                yolo_layer = YoloLayer(use_cuda=self.use_cuda)
                anchors = block['anchors'].split(',')
                anchor_mask = block['mask'].split(',')
                yolo_layer.anchor_mask = [int(i) for i in anchor_mask]
                yolo_layer.anchors = [float(i) for i in anchors]
                yolo_layer.num_classes = int(block['classes'])
                yolo_layer.num_anchors = int(block['num'])
                yolo_layer.anchor_step = len(yolo_layer.anchors)//yolo_layer.num_anchors
                try:
                    yolo_layer.rescore = int(block['rescore'])
                except:
                    pass
                yolo_layer.ignore_thresh = float(block['ignore_thresh'])
                yolo_layer.truth_thresh = float(block['truth_thresh'])
                yolo_layer.stride = prev_stride
                yolo_layer.nth_layer = ind
                yolo_layer.net_width = self.width
                yolo_layer.net_height = self.height
                out_filters.append(prev_filters)
                out_strides.append(prev_stride)
                models.append(yolo_layer)                
            else:
                print('unknown type %s' % (block['type']))
    
        return models

    def load_binfile(self, weightfile):
        fp = open(weightfile, 'rb')
       
        version = np.fromfile(fp, count=3, dtype=np.int32)
        version = [int(i) for i in version]
        if version[0]*10+version[1] >=2 and version[0] < 1000 and version[1] < 1000:
            seen = np.fromfile(fp, count=1, dtype=np.int64)
        else:
            seen = np.fromfile(fp, count=1, dtype=np.int32)
        self.header = torch.from_numpy(np.concatenate((version, seen), axis=0))
        self.seen = int(seen)
        body = np.fromfile(fp, dtype=np.float32)
        fp.close()
        return body

    def load_weights_roi(self, weightfile):
        ind = -2

        for block in self.blocks:
            ind += 1
            if block['type'] == 'net':
                continue
            elif block['type'] == 'roiclass':
                
                if self.use_cuda:
                    self.models[ind].load_state_dict(torch.load(weightfile+str(self.models[ind].class_size)+".weights", map_location=lambda storage, loc: storage.cuda()))
                else: 
                    self.models[ind].load_state_dict(torch.load(weightfile+str(self.models[ind].class_size)+".weights", map_location=torch.device('cpu')))

    def load_weights(self, weightfile):
        buf = self.load_binfile(weightfile)

        start = 0
        ind = -2
        for block in self.blocks:
            if start >= buf.size:
                break
            ind = ind + 1
            if block['type'] == 'net':
                continue
            elif block['type'] == 'convolutional':
                model = self.models[ind]
                batch_normalize = int(block['batch_normalize'])
                if batch_normalize:
                    start = load_conv_bn(buf, start, model[0], model[1])
                else:
                    start = load_conv(buf, start, model[0])
                    
    def save_weights_roi(self, outfile, cutoff=0):
        if cutoff <= 0:
            cutoff = len(self.blocks)-1

        dirname = os.path.dirname(outfile)
       
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        ind = -1
        for blockId in range(1, cutoff+1):
            ind = ind + 1
            block = self.blocks[blockId]

            if block['type'] == 'roiclass':
                model = self.models[ind]
                torch.save(model.state_dict(), outfile+str(model.class_size)+".weights")

    def save_weights(self, outfile, cutoff=0):
        if cutoff <= 0:
            cutoff = len(self.blocks)-1

        dirname = os.path.dirname(outfile)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        fp = open(outfile, 'wb')
        self.header[3] = self.seen
        header = np.array(self.header[0:3].numpy(), np.int32)
        header.tofile(fp)
        if (self.header[0]*10+self.header[1]) >= 2:
            seen = np.array(self.seen, np.int64)
        else:
            seen = np.array(self.seen, np.int32)
        seen.tofile(fp)

        ind = -1
        for blockId in range(1, cutoff+1):
            ind = ind + 1
            block = self.blocks[blockId]
            if block['type'] == 'convolutional':
                model = self.models[ind]
                batch_normalize = int(block['batch_normalize'])
                if batch_normalize:
                    save_conv_bn(fp, model[0], model[1])
                else:
                    save_conv(fp, model[0])
        fp.close()

#Test Bench: Success
if __name__ == '__main__':

    # #Initialize the model
    darknet = Darknet('/media/uncanny/e02b33a6-9c41-42f1-a2d3-95fbc3807f96/SHREEJAL_SYSTEM/Test/yolov3_roiclass/INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/yolov3_person_roi_new_arch.cfg')
    print(darknet.include_roi)
    print(darknet)
    proposals = torch.tensor([[0, 0, 10, 20, 30], [0, 15, 20, 25, 30]])
    dummy = torch.rand(1, 3, 608, 608)
   
    for i in range(100):
        _, out_fm = darknet(dummy)
        for i, l in enumerate(darknet.roi_layers):

            out = l(out_fm[i]['x'], proposals)    

