#!/usr/bin/python
# encoding: utf-8
from __future__ import division

import os
from PIL import Image, ImageFile, ImageDraw
import numpy as np
import cv2

# to avoid image file truncation error
ImageFile.LOAD_TRUNCATED_IMAGES = True

def scale_image_channel(im, c, v):
    cs = list(im.split())
    cs[c] = cs[c].point(lambda i: i * v)
    out = Image.merge(im.mode, tuple(cs))
    return out

def image_scale_and_shift(img, new_w, new_h, net_w, net_h, dx, dy):
    scaled = img.resize((new_w, new_h))
    # find to be cropped area
    sx, sy = -dx if dx < 0 else 0, -dy if dy < 0 else 0
    ex, ey = new_w if sx+new_w<=net_w else net_w-sx, new_h if sy+new_h<=net_h else net_h-sy
    scaled = scaled.crop((sx, sy, ex, ey))

    # find the paste position
    sx, sy = dx if dx > 0 else 0, dy if dy > 0 else 0
    assert sx+scaled.width<=net_w and sy+scaled.height<=net_h
    new_img = Image.new("RGB", (net_w, net_h), (127, 127, 127))
    new_img.paste(scaled, (sx, sy))
    del scaled
    return new_img

def image_scale_and_shift_nosafe(img, new_w, new_h, net_w, net_h, dx, dy):
    scaled = img.resize((new_w, new_h))
    new_img = Image.new("RGB", (net_w, net_h), (127, 127, 127))
    new_img.paste(scaled, (dx, dy))
    del scaled
    return new_img

def image_scale_and_shift_slow(img, new_w, new_h, net_w, net_h, dx, dy):
    scaled = np.array(img.resize((new_w, new_h)))
    # scaled.size : [height, width, channel]
    
    if dx > 0: 
        shifted = np.pad(scaled, ((0,0), (dx,0), (0,0)), mode='constant', constant_values=127)
    else:
        shifted = scaled[:,-dx:,:]

    if (new_w + dx) < net_w:
        shifted = np.pad(shifted, ((0,0), (0, net_w - (new_w+dx)), (0,0)), mode='constant', constant_values=127)
               
    if dy > 0: 
        shifted = np.pad(shifted, ((dy,0), (0,0), (0,0)), mode='constant', constant_values=127)
    else:
        shifted = shifted[-dy:,:,:]
        
    if (new_h + dy) < net_h:
        shifted = np.pad(shifted, ((0, net_h - (new_h+dy)), (0,0), (0,0)), mode='constant', constant_values=127)
    #print("scaled: {} ==> dx {} dy {} for shifted: {}".format(scaled.shape, dx, dy, shifted.shape))
    return Image.fromarray(shifted[:net_h, :net_w,:])
  
def distort_image(im, hue, sat, val):
    im = im.convert('HSV')
    cs = list(im.split())
    cs[1] = cs[1].point(lambda i: i * sat)
    cs[2] = cs[2].point(lambda i: i * val)
    
    def change_hue(x):
        x += hue*255
        if x > 255:
            x -= 255
        if x < 0:
            x += 255
        return x
    cs[0] = cs[0].point(change_hue)
    im = Image.merge(im.mode, tuple(cs))

    im = im.convert('RGB')
    #constrain_image(im)
    return im

def rand_scale(s):
    scale = np.random.uniform(1, s)
    if np.random.randint(2): 
        return scale
    return 1./scale

def random_distort_image(im, hue, saturation, exposure):
    dhue = np.random.uniform(-hue, hue)
    dsat = rand_scale(saturation)
    dexp = rand_scale(exposure)
    res = distort_image(im, dhue, dsat, dexp)
    return res

def data_augmentation_crop(img, shape, jitter, hue, saturation, exposure):
    oh = img.height  
    ow = img.width
    
    dw =int(ow*jitter)
    dh =int(oh*jitter)

    pleft  = np.random.randint(-dw, dw)
    pright = np.random.randint(-dw, dw)
    ptop   = np.random.randint(-dh, dh)
    pbot   = np.random.randint(-dh, dh)

    swidth =  ow - pleft - pright
    sheight = oh - ptop - pbot

    sx = ow / float(swidth)
    sy = oh / float(sheight)
    
    flip = np.random.randint(2)

    cropbb = np.array([pleft, ptop, pleft + swidth - 1, ptop + sheight - 1])
    # following two lines are old method. out of image boundary is filled with black (0,0,0)
    #cropped = img.crop( cropbb )
    #sized = cropped.resize(shape)

    nw, nh = cropbb[2]-cropbb[0], cropbb[3]-cropbb[1]
    # get the real image part
    cropbb[0] = -min(cropbb[0], 0)
    cropbb[1] = -min(cropbb[1], 0)
    cropbb[2] = min(cropbb[2], ow)
    cropbb[3] = min(cropbb[3], oh)
    cropped = img.crop( cropbb )

    # calculate the position to paste
    bb = (pleft if pleft > 0 else 0, ptop if ptop > 0 else 0)
    new_img = Image.new("RGB", (nw, nh), (127,127,127))
    new_img.paste(cropped, bb)

    sized = new_img.resize(shape)
    del cropped, new_img
    
    dx = (float(pleft)/ow) * sx
    dy = (float(ptop) /oh) * sy

    if flip: 
        sized = sized.transpose(Image.FLIP_LEFT_RIGHT)
    img = random_distort_image(sized, hue, saturation, exposure)
    # for compatibility to nocrop version (like original version)
    return img, flip, dx, dy, sx, sy 

def data_augmentation_nocrop(img, shape, jitter, hue, sat, exp):
    net_w, net_h = shape
    img_w, img_h = img.width, img.height
        
    # determine the amount of scaling and cropping
    dw = jitter * img_w
    dh = jitter * img_h

    new_ar = (img_w + np.random.uniform(-dw, dw)) / (img_h + np.random.uniform(-dh, dh))
    # scale = np.random.uniform(0.25, 2)
    scale = 1.

    if (new_ar < 1):
        new_h = int(scale * net_h)
        new_w = int(net_h * new_ar)
    else:
        new_w = int(scale * net_w)
        new_h = int(net_w / new_ar)
            
    dx = int(np.random.uniform(0, net_w - new_w))
    dy = int(np.random.uniform(0, net_h - new_h))
    sx, sy = new_w / net_w, new_h / net_h
        
    # apply scaling and shifting
    new_img = image_scale_and_shift(img, new_w, new_h, net_w, net_h, dx, dy)
        
    # randomly distort hsv space
    new_img = random_distort_image(new_img, hue, sat, exp)
        
    # randomly flip
    flip = np.random.randint(2)
    if flip: 
        new_img = new_img.transpose(Image.FLIP_LEFT_RIGHT)
            
    dx, dy = dx/net_w, dy/net_h
    return new_img, flip, dx, dy, sx, sy 

def chang_coord(bs, sx, dx, sy, dy, flip):
    x1 = bs[1] - bs[3]/2
    y1 = bs[2] - bs[4]/2
    x2 = bs[1] + bs[3]/2
    y2 = bs[2] + bs[4]/2
    
    x1 = min(0.999, max(0, x1 * sx - dx)) 
    y1 = min(0.999, max(0, y1 * sy - dy)) 
    x2 = min(0.999, max(0, x2 * sx - dx))
    y2 = min(0.999, max(0, y2 * sy - dy))
    
    bs[1] = (x1 + x2)/2 # center x
    bs[2] = (y1 + y2)/2 # center y
    bs[3] = (x2 - x1)   # width
    bs[4] = (y2 - y1)   # height

    if flip:
        bs[1] =  0.999 - bs[1] 

    return bs

def fill_truth_detection(labpath, crop, flip, dx, dy, sx, sy, index):
    max_boxes = 50
    max_proposals = 100
    label = np.zeros((max_boxes, 5))
    proposals = np.zeros((max_proposals, 5))
    label_proposals = np.zeros((max_proposals, 2))
    if os.path.getsize(labpath):
        bs = np.loadtxt(labpath)
        bs = bs if bs.ndim > 1 else bs.reshape(1, -1)
        bs_label = bs[np.where(bs[:, 0] == 0.)[0]]
        bs_pos = bs[np.where(bs[:, 0] == 1.)[0]]
        bs_neg = bs[np.where(bs[:, 0] == -1.)[0]]
        #######################For Original Labels######################################
        if bs_label is None:
            return label
        bs_label = np.reshape(bs_label, (-1, 5))
        cc = 0
        for i in range(bs_label.shape[0]):
            bs_label[i] = chang_coord(bs_label[i], sx, dx, sy, dy, flip)
            
            # when crop is applied, we should check the cropped width/height ratio
            if bs_label[i][3] < 0.002 or bs_label[i][4] < 0.002 or \
                (crop and (bs_label[i][3]/bs_label[i][4] > 20 or bs_label[i][4]/bs_label[i][3] > 20)):
                continue
            
            #label_proposals[cc][0] = 1
            #proposals[cc] = bs_label[i]
            #proposals[cc][0] = index
            label[cc] = bs_label[i]
            cc += 1
            if cc >= 50:
                break
    
        ##############For Synthetic Positive Labels#######################################
        cc_p = 0
        if bs_pos.shape[0] != 0:
            
            bs_pos = np.reshape(bs_pos, (-1, 5))
            for i in range(bs_pos.shape[0]):
    
                bs_pos[i] = chang_coord(bs_pos[i], sx, dx, sy, dy, flip)
    
                # when crop is applied, we should check the cropped width/height ratio
                if bs_pos[i][3] < 0.002 or bs_pos[i][4] < 0.002 or \
                    (crop and (bs_pos[i][3]/bs_pos[i][4] > 20 or bs_pos[i][4]/bs_pos[i][3] > 20)):
                    continue
    
                proposals[cc_p] = bs_pos[i]
                proposals[cc_p][0] = index
                label_proposals[cc_p][0] = 1
    
                cc_p += 1
                if cc_p >= 100:
                    break
        
        ##################For Synthetic Negative Labels#############################################
        if bs_neg.shape[0] != 0:
            bs_neg = np.reshape(bs_neg, (-1, 5))
            for i in range(bs_neg.shape[0]):
    
                bs_neg[i] = chang_coord(bs_neg[i], sx, dx, sy, dy, flip)
                
                # when crop is applied, we should check the cropped width/height ratio
                if bs_neg[i][3] < 0.002 or bs_neg[i][4] < 0.002 or \
                    (crop and (bs_neg[i][3]/bs_neg[i][4] > 20 or bs_neg[i][4]/bs_neg[i][3] > 20)):
                    continue
                
                proposals[cc_p] = bs_neg[i]
                proposals[cc_p][0] = index
                label_proposals[cc_p][1] = 1
                
                cc_p += 1
                if cc_p >= 100:
                    break 
    
    label = np.reshape(label, (-1, 5))
    return label, label_proposals, proposals

def letterbox_image(img, net_w, net_h):
    im_w, im_h = img.size
    if float(net_w)/float(im_w) < float(net_h)/float(im_h):
        new_w = net_w
        new_h = (im_h * net_w)//im_w
    else:
        new_w = (im_w * net_h)//im_h
        new_h = net_h
    resized = img.resize((new_w, new_h), Image.ANTIALIAS)
    lbImage = Image.new("RGB", (net_w, net_h), (127,127,127))
    lbImage.paste(resized, \
            ((net_w-new_w)//2, (net_h-new_h)//2, \
             (net_w+new_w)//2, (net_h+new_h)//2))
    return lbImage

def correct_proposal_boxes(boxes, im_w, im_h, net_w, net_h):
    
    im_w, im_h = float(im_w), float(im_h)

    net_w, net_h = float(net_w), float(net_h)

    if net_w/im_w < net_h/im_h:
        new_w = net_w
        new_h = (im_h * net_w)/im_w
    else:
        new_w = (im_w * net_h)/im_h
        new_h = net_h

    xo, xs = (net_w - new_w)/(2*net_w), new_w/net_w
    yo, ys = (net_h - new_h)/(2*net_h), new_h/net_h
    
    for i in range(len(boxes)):
        b = boxes[i] 
        b[0] = b[0] * xs + xo
        b[1] = b[1] * ys + yo
        b[2] *= xs
        b[3] *= ys
    return

def correct_yolo_boxes(boxes, im_w, im_h, net_w, net_h):
    
    im_w, im_h = float(im_w), float(im_h)
    net_w, net_h = float(net_w), float(net_h)

    if net_w/im_w < net_h/im_h:
        new_w = net_w
        new_h = (im_h * net_w)/im_w
    else:
        new_w = (im_w * net_h)/im_h
        new_h = net_h
    xo, xs = (net_w - new_w)/(2*net_w), net_w/new_w
    yo, ys = (net_h - new_h)/(2*net_h), net_h/new_h
    
    for i in range(len(boxes)):
        b = boxes[i] 
        b[0] = (b[0] - xo) * xs
        b[1] = (b[1] - yo) * ys
        b[2] *= xs
        b[3] *= ys
    return

def load_data_detection(imgpath, shape, crop, jitter, hue, saturation, exposure, index):
    labpath = imgpath.replace('images', 'labels').replace('JPEGImages', 'coco_bbox').replace('.jpg', '.txt').replace('.png','.txt')
    
    ## data augmentation
    img = Image.open(imgpath).convert('RGB')
    if crop:         # marvis version
        img,flip,dx,dy,sx,sy = data_augmentation_crop(img, shape, jitter, hue, saturation, exposure)
    else:            # original version
        img,flip,dx,dy,sx,sy = data_augmentation_nocrop(img, shape, jitter, hue, saturation, exposure)
    
    label, label_proposals, proposals = fill_truth_detection(labpath, crop, flip, -dx, -dy, sx, sy, index)

    #Convert into [TLx, TLy, BRx, BRy] format
    proposals = convert_from_YOLO(proposals, shape[0], shape[1])
    return img, label, label_proposals, proposals


def convert_from_YOLO(bbox, width, height):
    _, xc, yc, w, h = np.split(bbox, 5, axis=1)
    
    xmin = (xc * width)  - (w * width) / 2.        
    xmax = xmin + (w * width)

    ymin = (yc * height)  - (h * height) / 2.    
    ymax = ymin + (h * height)

    return np.hstack((_, np.clip(xmin, 0, width), np.clip(ymin, 0, height), np.clip(xmax, 0, width), np.clip(ymax, 0, height))).astype(int)

def draw_bbox(image, bbox, color=(255, 0, 0)):
    image = ImageDraw.Draw(image)
    for bb in bbox.astype(int):
        image.rectangle(((bb[0], bb[1]), (bb[2], bb[3])), outline=color, width=3)

def debug(img, label, label_proposals, proposals, filename, width, height):
    bbox = proposals[np.where(label_proposals[:,0] == 1)][:,1:]
    print("Total Valid Positive Samples: %d"%bbox.shape[0])
    draw_bbox(img, bbox, color='red')
    bbox = proposals[np.where(label_proposals[:,1] == 1)][:,1:]
    print("Total Valid Negative Samples: %d"%bbox.shape[0])
    draw_bbox(img, bbox, color='blue')
    img.save(os.path.join("debug_scripts/debug_train_data", filename))

#Test Bench SUCCESS
if __name__ == '__main__':

    with open("train.txt", "r") as f:
        total_files = f.readlines()
        files = [fi.strip("\n") for fi in total_files]
    f.close()

    for imgpath in files:
        img, label, label_proposals, proposals = load_data_detection(imgpath, shape=(608, 608), crop=False, jitter=0.3, hue=0.1, saturation=1.5, exposure=1.5, index = 0)
        print("Filename: %s"%imgpath)
        filename = os.path.basename(imgpath)
        debug(img, label, label_proposals, proposals, filename, 608, 608)
