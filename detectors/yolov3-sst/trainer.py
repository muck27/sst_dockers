'''
Author: Shreejal Trivedi

Date: 30th July 2020

Description: A trainer class for SST Branch Training YOLOv3 + ROIClass.
             Contains training and validation functions for both Classifier and ROIClass
             
             Feature Added: Multiscale training of the classifier branch. 

'''

from __future__ import print_function
from __future__ import division
import sys
import time
import torch
import torch.optim as optim
import torch.nn as nn
from torchvision import datasets, transforms
import gc
import dataset
from utils import *
from image import correct_yolo_boxes
from cfg import parse_cfg
from darknet_v2 import Darknet
import argparse
import itertools
import random


class YOLOv3ROIClassTrainer():

    def __init__(self, FLAGS, FREEZE_DETECTOR=True, device=None, use_cuda=None, multiscale=True, save_interval=10, test_interval=1, backup_save=2, evaluate=True, conf_thresh=0.25, nms_thresh=0.4, iou_thresh=0.5):

        ''' 
        Description: Initialization Configurations of the ROICLASS Trainer
        
                     Compulory argument: FLAGS contains netowork initializations and configuration parameters such as cfgfile, datafile, etc.
                     Optional arguments: Can be passed and initialized according to the needs and site specified.
                    
        '''

        # Initialize Parameters

        self.datacfg = FLAGS.data
        self.cfgfile = FLAGS.config
        self.weightfile = FLAGS.weights
        self.weightfile_roi = FLAGS.weights_roi

        self.FREEZE_DETECTOR = FREEZE_DETECTOR
        self.nms_thresh = nms_thresh
        self.iou_thresh = iou_thresh
        self.save_interval = save_interval
        self.evaluate = evaluate
        self.conf_thresh = conf_thresh
        self.test_interval = test_interval
        self.backup_save = backup_save
        self.multiscale = multiscale

        # Parse Datafile CFG
        
        data_options = read_data_cfg(self.datacfg)
        self.trainlist = data_options['train']
        self.testlist = data_options['valid']
        self.backupdir = data_options['backup']
        self.gpus = data_options['gpus']
        #self.ngpus = len(self.gpus.split(','))
        self.ngpus = 0
        self.num_workers = int(data_options['num_workers'])

        # Parse Network CFG
        net_options = parse_cfg(self.cfgfile)[0]
        self.batch_size = int(net_options['batch'])
        self.max_batches = int(net_options['max_batches'])
        self.learning_rate = float(net_options['learning_rate'])
        self.momentum = float(net_options['momentum'])
        self.decay = float(net_options['decay'])
        self.steps = [float(step) for step in net_options['steps'].split(',')]                              
        self.scales = [float(scale) for scale in net_options['scales'].split(',')]

        # Pre-requisites for GPU if any

        self.use_cuda = torch.cuda.is_available() and (True if use_cuda is None else use_cuda)
        print(self.use_cuda)
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        self.device = torch.device('cuda' if self.use_cuda else 'cpu')

        try:
            self.max_epochs = int(net_options['max_epochs'])
        except KeyError:
            nsamples = file_lines(self.trainlist)
            self.max_epochs = (self.max_batches * self.batch_size) // nsamples + 1
   
        self.seed = int(time.time())
        torch.manual_seed(self.seed)
        if use_cuda:
            os.environ['CUDA_VISIBLE_DEVICES'] = self.gpus
            torch.cuda.manual_seed(self.seed)

        # Initialize YOLOv3 + ROIClass Model
        self.initialize_model(FLAGS.reset)

        # Initialize Test Loader
        # self.load_testlist()

        params_dict = dict(self.model.named_parameters())
        params = []
        for key, value in params_dict.items(): 
            if key.find('.bn') >= 0 or key.find('.bias') >= 0:
                params += [{'params': [value], 'weight_decay': 0.0}]
            else:
                params += [{'params': [value], 'weight_decay': self.decay * self.batch_size}]

        # Initialize optimizer                                                                               
        self.optimizer = optim.SGD(self.model.parameters(), 
                        lr=self.learning_rate / self.batch_size, momentum=self.momentum, 
                        dampening=0, weight_decay=self.decay * self.batch_size)    

            
    def initialize_model(self, reset):


        '''
        Description: Initialization of the branch + detector network
                    Contains some prerequisite function calls/ declarations for the initialization
                    of the model. 
        '''

        # Some pre-requisites for ROIClass
        print("Num workers: %d"%self.num_workers)
        self.max_proposals = 100
        self.proposal_batch_size = self.batch_size
        self.roi_criterion = nn.CrossEntropyLoss()
        self.weight_roi = 1

        # Declare YOLO Model

        self.model = Darknet(self.cfgfile, use_cuda=self.use_cuda)

        # Load Pretrained YOLO weights and ROIClass weights if any
        
        if self.weightfile is not None:
            self.model.load_weights(self.weightfile)

        if len(self.model.roi_layers) > 0 and self.weightfile_roi is not None:
            print('Loading Pretrained ROI Weights')
            self.model.load_weights_roi(self.weightfile_roi)
       
        if len(self.model.roi_layers) > 0:
            self.avg_loss_per_layer = np.empty(len(self.model.roi_layers))
            self.avg_loss_per_layer.fill(-1.)

        self.avg_loss = -1
    
        # Freeze the detector if FREEZE_DETECTOR is True

        if self.FREEZE_DETECTOR and (self.model.roi_layers) > 0:
            print('Freezing the detector ...')
            roiclass_list = ["models." + str(idx) for idx in self.model.roi_index]
            for name, params in self.model.named_parameters():        
                if not any(subroiclass in name for subroiclass in roiclass_list):
                    params.requires_grad = False

            # Check parameters which are not freezed
            print("Checking for unfreezed layers: ROICLASS Index: ", self.model.roi_index)
            for name, params in self.model.named_parameters():
                if params.requires_grad is True:
                    print(name)

        # Initialize the Model
        
        nsamples = file_lines(self.trainlist)
        
        if reset:
            print('Resetting the model')
            self.model.seen = 0
            self.init_epoch = 0
        else:
            self.init_epoch = self.model.seen // nsamples

        self.loss_layers = self.model.loss_layers
        for l in self.loss_layers:
            l.seen = self.model.seen
        
        self.roi_layers = self.model.roi_layers
        for l in self.roi_layers:
            l.seen = self.model.seen
        
        if len(self.roi_layers) == 0:
            print("No ROI Class Present ...")

        # Transfer the model to GPU if cuda flag is True
        print(self.use_cuda)
        print('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy')
    
        if self.use_cuda:
            if self.ngpus > 1:
                self.model = torch.nn.DataParallel(self.model).to(self.device)
            else:
                self.model = self.model.to(self.device)
    
        print(self.ngpus)
    def load_testlist(self, shape):
        
        '''
        Description: Data Loader for validation of the model. Called in test() function

        '''
        if self.multiscale:
            init_width = shape[0]
            init_height = shape[1]
        else:
            init_width = self.model.width
            init_height = self.model.height
        
        print("Test loader ready..!")
        kwargs = {'num_workers': self.num_workers, 'pin_memory': True} if self.use_cuda else {}
        self.test_loader = torch.utils.data.DataLoader(
            dataset.listDataset(self.testlist, shape=(init_width, init_height),
                        shuffle=False,
                        transform=transforms.Compose([
                            transforms.ToTensor(),
                        ]), train=False),
            batch_size=1, shuffle=False, **kwargs)
    
    def curmodel(self):
        
        if self.ngpus > 1:
            cur_model = self.model.module
        else:
            cur_model = self.model

        return cur_model

    def adjust_learning_rate(self, batch):

        '''
        Sets the learning rate to the initial LR decayed by 10 every 30 epochs
        
        '''
        lr = self.learning_rate
        for i in range(len(self.steps)):
            scale = self.scales[i] if i < len(self.scales) else 1

            if batch >= self.steps[i]:
                lr = lr * scale
                if batch == self.steps[i]:
                    break
            else:
                break

        for param_group in self.optimizer.param_groups:
            param_group['lr'] = lr / self.batch_size
        
        return lr

    def start_training(self):

        '''
        Main Function to start Training of YOLOv3 +/ - ROIClass. Call this function after the class initialization
        directly to start the branh and/or main backbone training.

        '''

        try:
            print("Training for ({:d},{:d})".format(self.init_epoch + 1, self.max_epochs))

            for epoch in range(self.init_epoch + 1, self.max_epochs + 1):
                print("Epoch No: %d | Model Seen: %d" % (epoch, self.model.seen))
                nsamples = self.train(epoch)
                self.model.seen += epoch * nsamples

                if epoch % self.backup_save == 0:
                    save_epoch = True if epoch % self.save_interval == 0 else False
                    self.savemodel(epoch, nsamples, save_epoch=save_epoch)

                if self.evaluate and epoch >= self.test_interval and (epoch % self.test_interval) == 0:
                    self.test(epoch)


        except KeyboardInterrupt:
            print('='*80)
            print('Exiting from training by interrupt')

    def savemodel(self, epoch, nsamples, save_epoch=False):

        '''
        Description: Save ROI and Detector weights at every save_interval period.
                     Default directory is specified in backupdir in datafile

        '''
        
        cur_model = self.curmodel()
        cur_model.seen = epoch * nsamples

        if not self.FREEZE_DETECTOR:
            
            logging('save weights to %s/%06d.weights' % (self.backupdir, epoch))
            cur_model.save_weights('%s/%06d.weights' % (self.backupdir, epoch))

            if len(self.roi_layers) > 0:
                logging('save weights_roi to %s/%06d_roi.weights' % (self.backupdir, epoch))
                cur_model.save_weights_roi('%s/%06d_roi_' % (self.backupdir, epoch))
        else:
            if len(self.roi_layers) > 0:

                logging('save weights to %s/%s.weights' % (self.backupdir, "backup"))
                cur_model.save_weights('%s/%s.weights' % (self.backupdir, "backup"))
                logging('save weights_roi to %s/%s_roi.weights' % (self.backupdir, "backup"))
                cur_model.save_weights_roi('%s/%s_roi_' % (self.backupdir, "backup"))

                if save_epoch:
                    logging('save weights to %s/%06d.weights' % (self.backupdir, epoch))
                    cur_model.save_weights('%s/%06d.weights' % (self.backupdir, epoch))
                    logging('save weights_roi to %s/%06d_roi.weights' % (self.backupdir, epoch))
                    cur_model.save_weights_roi('%s/%06d_roi_' % (self.backupdir, epoch))
                
    
    def test(self, epoch):

        ''' 
        Description: Validation Function for ROIClass + Detector Network.
                     Detector validation is only done if the main network branch is unfreezed
                     All the logs of the validation are written in savelog.txt

        '''

        wh = (random.randint(0, 9) + 10) * 32
        self.load_testlist(shape=(wh, wh))

        test_width = wh if self.multiscale else self.model.width

        def truths_length(truths):
            for i in range(50):
                if truths[i][1] == 0:
                    return i
            return 50
        
        print('Test Module Started Epoch No: %d Test Resolution: %d. Generating Test Confusion Matrix...' % (epoch, test_width))
        
        self.model.eval()
        cur_model = self.curmodel()
        num_classes = cur_model.num_classes
        total = 0.0
        eps = 1e-5
        proposals = 0.0
        correct = 0.0 

        # ROIClass Accuracy Calculation
        truth_list = [list() for _ in range(len(self.roi_layers))]
        pred_list = [list() for _ in range(len(self.roi_layers))]

        shape = (cur_model.width, cur_model.height)

        with torch.no_grad():

            for idx, (data, target, org_w, org_h, _, target_prop, prop) in enumerate(self.test_loader):
                data = data.to(self.device)
                if (self.model.roi_layers) > 0:
                    output, output_fm = self.model(data)
                else:
                    output = self.model(data)

                # For ROI CLASS
                if len(self.roi_layers) > 0: # ROIClass Validation

                    prop = torch.reshape(prop, (prop.shape[0] * prop.shape[1], 5))
                    target_prop = torch.reshape(target_prop, (target_prop.shape[0] * target_prop.shape[1], 1))
                    
                    for i, l in enumerate(self.roi_layers):
                                            
                        prop_filters, target_prop_filters = l.candidate_proposals(prop, target_prop)
                        
                        if prop_filters.shape[0] > 0:
                            out = l(output_fm[i]['x'], prop_filters)
                            pred = np.argmax(out.data.cpu().numpy(), axis=1)
                            truth = target_prop_filters.cpu().numpy().astype(int).reshape(-1)
                            pred_list[i].append(pred)
                            truth_list[i].append(truth)
                
                if not self.FREEZE_DETECTOR: # Detector Validation
                    
                    use_class = True if len(self.roi_layers) > 0 else False
                    all_boxes = get_all_boxes(output, shape, self.conf_thresh, num_classes, use_cuda=self.use_cuda, use_class=use_class)
                    
                    for k in range(len(all_boxes)):
                        boxes = all_boxes[k]
                        correct_yolo_boxes(boxes, org_w[k], org_h[k], cur_model.width, cur_model.height)
                        boxes = np.array(nms(boxes, self.nms_thresh))

                        truths = target[k].view(-1, 5)
                        num_gts = truths_length(truths)
                        total = total + num_gts
                        num_pred = len(boxes)
                        if num_pred == 0:
                            continue

                        proposals += int((boxes[:, 4] > self.conf_thresh).sum())
                        for i in range(num_gts):
                            gt_boxes = torch.FloatTensor([truths[i][1], truths[i][2], truths[i][3], truths[i][4], 1.0, 1.0, truths[i][0]])
                            gt_boxes = gt_boxes.repeat(num_pred, 1).t()
                            pred_boxes = torch.FloatTensor(boxes).t()
                            best_iou, best_j = torch.max(multi_bbox_ious(gt_boxes, pred_boxes, x1y1x2y2=False), 0)

                            if best_iou > self.iou_thresh and pred_boxes[6][best_j] == gt_boxes[6][0]:
                                correct += 1

                if (idx + 1) % 200 == 0:
                    print("End of testing %d samples"% (idx + 1))
       
        if len(self.roi_layers) > 0:
        
            for i in range(len(self.roi_layers)): # Calculation of Precision/Recall Confusion Matrix of ROIClass

                truth_ls = np.array(list(itertools.chain.from_iterable(truth_list[i])))
                pred_ls = np.array(list(itertools.chain.from_iterable(pred_list[i])))
                
                # Calculating Precision-Recall
                tp = np.sum(np.logical_and(truth_ls == 1, pred_ls == 1))
                tn = np.sum(np.logical_and(truth_ls == 0, pred_ls == 0))
                fp = np.sum(np.logical_and(truth_ls == 0, pred_ls == 1))
                fn = np.sum(np.logical_and(truth_ls == 1, pred_ls == 0))
            
                # For Foreground
                pr1 = tp / (tp + fp + eps)
                re1 = tp / (tp + fn + eps)
                f1score = 2 * pr1 * re1 / (pr1 + re1 + eps)

                # For Background
                pr0 = tn / (tn + fn + eps)
                re0 = tn / (tn + fp + eps)
                f0score = 2 * pr0 * re0 / (pr0 + re0 + eps)

                savelog("[%03d] RESOLUTION: [%d x %d] Class: %s Total Examples: %d Foreground: TPs: %d, FPs: %d | roi_precision: %.3f, roi_recall: %.3f, roi_fsocre: %.3f" % (epoch, test_width, test_width, self.roi_layers[i].class_size, sum(truth_ls == 1), tp, fp, pr1, re1, f1score))
                savelog("[%03d] RESOLUTION: [%d x %d] Class: %s Total Examples: %d Background: TNs: %d, FNs: %d | roi_precision: %.3f, roi_recall: %.3f, roi_fsocre: %.3f" % (epoch, test_width, test_width, self.roi_layers[i].class_size, sum(truth_ls == 0), tn, fn, pr0, re0, f0score))
        
        if not self.FREEZE_DETECTOR: # Calculation of Precision/Recall Confusion Matrix of Detector Branch

            precision = 1.0 * correct / (proposals + eps)
            recall = 1.0 * correct / (total + eps)
            fscore = 2.0 * precision * recall / (precision + recall + eps)

            savelog("[%03d] correct: %d, precision: %f, recall: %f, fscore: %f" % (epoch, correct, precision, recall, fscore))
        
        del self.test_loader

    def train(self, epoch):

    
        '''
        
        Train Main Function ::: Backward propogation of ROIClass + YOLOv3 
        Proposal Branch Forward Propogation

        Branch Forward Propogation support multiscale training i.e small, medium, large. 
        You can specify in the configuration file of the network about the same. 

        '''
        self.model.train()
        cur_model = self.curmodel()
        init_width = cur_model.width
        init_height = cur_model.height
        kwargs = {'num_workers': self.num_workers, 'pin_memory': True} if self.use_cuda else {}
        
        # Train Data Loader. To customize the dataloader, edit dataset.py and image.py file
        
        train_loader = torch.utils.data.DataLoader(
                    dataset.listDataset(self.trainlist, 
                                        shape=(init_width, init_height),
                                        shuffle=True,
                                        transform=transforms.Compose([
                                            transforms.ToTensor(),
                                            ]), 
                                        train=True,
                                        seen=0,
                                        multiscale=self.multiscale,
                                        batch_size=self.batch_size,
                                        num_workers=self.num_workers),
                    collate_fn=dataset.custom_collate, 
                    batch_size=self.batch_size, shuffle=False, **kwargs)

        self.processed_batches = cur_model.seen // self.batch_size

        lr = self.adjust_learning_rate(self.processed_batches)
        logging('[%03d] processed %d samples, lr %e' % (epoch, epoch * len(train_loader.dataset), lr))
        self.model.train()

        for batch_idx, (data, target, label_proposals, proposals) in enumerate(train_loader):

            self.adjust_learning_rate(self.processed_batches)
            self.processed_batches = self.processed_batches + 1
        
            data, target = data.to(self.device), target.to(self.device)
            
            # ROIClass Proposals 
            proposals, label_proposals = proposals.to(self.device), label_proposals.to(self.device)

            self.optimizer.zero_grad()

            # Forward Propogation on Detector/Classifer Branch
            if len(self.roi_layers) > 0:
                output, out_fm = self.model(data)
            else:
                output = self.model(data)
                

            if not self.FREEZE_DETECTOR:
                org_loss = []
                for i, l in enumerate(self.loss_layers):
                    l.seen = l.seen + data.data.size(0)
                    ol = l(output[i]['x'], target)
                    org_loss.append(ol) 

            if len(self.roi_layers) > 0:

                # Gather all the proposals and keep only top K proposals. K can be changed in max_proposals variable
                total_props = 0        
                proposals = torch.reshape(proposals, (proposals.shape[0] * self.max_proposals, 5))
                label_proposals = torch.reshape(label_proposals, (label_proposals.shape[0] * self.max_proposals, 2))
                proposals, label_proposals = self.model.sample_proposals(proposals, label_proposals) # Delete all empty samples from the proposals(Max=100)
                roi_loss = []
                
                for i, l in enumerate(self.roi_layers):
                    roi_loss_per_layer = []
                    
                    l.seen = l.seen + data.data.size(0)
                    proposals_filters, label_proposals_filters = l.candidate_proposals(proposals, label_proposals)
                    batch_range = np.arange(start=0, stop=proposals_filters.shape[0], step=self.proposal_batch_size)
                    batch_range = batch_range if proposals_filters.shape[0] % self.proposal_batch_size == 0 else batch_range[:-1]
                    total_props += (proposals_filters.shape[0] - (proposals_filters.shape[0] % self.proposal_batch_size))
                    
                    for j in batch_range: # Minibatch for Classifier: Default is batch size of detector.
                        out = l(out_fm[i]['x'], proposals_filters[j:j + self.proposal_batch_size])
                        loss = self.roi_criterion(out, label_proposals_filters[j:j + self.proposal_batch_size].long())
                        roi_loss.append(loss)
                        roi_loss_per_layer.append(loss.detach().item())
                    
                    loss_full_per_layer = sum(roi_loss_per_layer) / self.batch_size
                    self.avg_loss_per_layer[i] = loss_full_per_layer if self.avg_loss_per_layer[i] < 0 else self.avg_loss_per_layer[i] * 0.9 + loss_full_per_layer * 0.1
                    print("layer id: %d avg_loss: %f roiloss: %f nPropPerClass: %d class size: %s data size: %d" % (self.model.roi_index[i], self.avg_loss_per_layer[i], loss_full_per_layer, proposals_filters.shape[0] - (proposals_filters.shape[0] % self.proposal_batch_size), l.class_size, data.data.size(2)))        
                
                # Per-batch Loss works preety good than per-proposal loss
                # loss_full = sum(roi_loss) / (proposals.shape[0] - proposals.shape[0] % batch_size)
                        
                loss_full = sum(roi_loss) / self.batch_size
                self.avg_loss = loss_full if self.avg_loss < 0 else self.avg_loss * 0.9 + loss_full * 0.1

                print("%d total_avg_roiloss: %f total_roiloss: %f nProp: %d "%((batch_idx + 1) * self.batch_size, self.avg_loss, loss_full, total_props))

            # If YOLO Detector is freezed. Only do backward loss on Classifier Loss
            if self.FREEZE_DETECTOR:

                if len(self.roi_layers) > 0 and len(roi_loss) > 0:        
                    (loss_full * self.weight_roi).backward()
            else:
                # Total Loss: YOLO Loss + ROICLASS Loss(Please change weight ROI value: Default is 1--->Giving good convergence)
                
                if len(self.roi_layers) > 0: 
                    (sum(org_loss) + loss_full * self.weight_roi).backward()
                else:
                    sum(org_loss).backward()
    
            nn.utils.clip_grad_norm_(self.model.parameters(), 10000)
            
            self.optimizer.step()

            del data, target, proposals, label_proposals
            org_loss = []
            roi_loss = []
            roi_loss_per_layer = []
            gc.collect()

        nsamples = len(train_loader.dataset)
        del train_loader
        return nsamples
