# YOLOv3 + ROICLASS: Site Specific Training #

This work introduces a method to leverage a lightweight classifier with the dense object detectors such as YOLOv3. ROICLass is a lightweight classifier module that takes the detector proposals
as input and classify them into two classes i.e background and foreground class. More specifically, they are used to used to remove the false positives that the classifier may detect at lower 
confidence thresholds.

___

### Birdview working of the inference pipeline ###

1. START
2. INTIALIZE the threshold values `min_prop_thresh`, `conf_thresh`, `max_prop_thresh`
2. Forward pass the input image to the network
3. Gather all the detection result outputs at confidence threshold = `conf_thresh`
4. Seggregate the outputs and take only those boxes whose confidence is between `min_prop_thresh` and `max_prop_thresh`
5. Pass the proposals obtained from Step. 4 to the classifier branch. 
6. Remove all the boxes that are classified as a Background(or Non-Person) Class.
7. Increase the confidence of the box that is classified as a Person Class to some high value near `max_prop_thresh`.
8. Consider only those boxes that are classifed as a Foreground Class and whose box confidence value is greater than `max_prop_thresh`.
9. STOP

___

### About this repository ###

This repository contains the inference code of the detector+classifier pipeline.`yolov3_roiclass.py` is the class definition that contains the inference flow of the classification 
and detection branch. `test_detector_roiclass.py` is the main function script calling and initializing the inference class `YOLOv3ROIClassTester`. 

* Walkthrough of the repository's inference code snippets.

1. `cfg.py`: This file contains some basic parsing utility functions of network configuration. 
2. `darknet.py`: Network definition class of YOLOv3(deprecated). 
3. `darknet_v2.py`: New verison of the class definition of YOLOv3. This file contains the implementation of the attention layers moduels such as `SAM`, `BAM`, `CBAM` and `FPN Arch ~3% Accuracy Gains`. 
4. `dataset.py`: Training data generation script. Only used while training the detection and classfier brach.
5. `image.py`: Contains the utility functions for baseline image proccessing while inference. 
6. `yolov3_roiclass.py`: Inference class definition of YOLOv3+ROIClass pipeline. This function can be called and initialized from any file. Main function is `run_detector(frame, imagename)`. You can modify 
	this function according to the requirements. # Deprecated
7. `test_detector_roiclass.py`: Main call to the inference code. `Testbench = SUCCESS`.
8. `trainer.py`: Contains the utility functions for training the Branch Classifier. A Class wrapper is defined that can be called directly from any file. To start the training, call `start_training()` function.
9. `tester.py`: New inference class definition of YOLOv3 SST. Added new algorithm for change detector integration.
9. `train_detector_roiclass.py`: Main call to the training code. `Testbench = SUCCESS`
10. `validator.py`: Main call to get the accuracy numbers on ROIFormat validation data. `TestBench = SUCCESS`
11. `bgsrunner.py`: Class definition of BGS Motion Detection pipeline support with YOLOv3 SST. `TestBench = SUCCESS`

Darknet YOLOv3 code is forked from Andy Yun's repository: [Github](https://github.com/andy-yun/pytorch-0.4-yolov3)
___

### How to run the inference script ###

Python script: 	`test_detector_roiclass.py`

Usage: 

```
$ python test_detector_roiclass.py --help	
test_detector_roiclass.py [-h] [--video_feed VIDEO_FEED]
                                 [--video VIDEO] [--data DATA]
                                 [--config CONFIG]
                                 [--weights WEIGHTS [WEIGHTS ...]]
                                 [--roi_weights ROI_WEIGHTS [ROI_WEIGHTS ...]]
                                 [--bgs BGS] [--conf_thresh CONF_THRESH]
                                 [--use_class USE_CLASS]

optional arguments:
  -h, --help            show this help message and exit
  --video_feed VIDEO_FEED, -vf VIDEO_FEED
                        Video/Frame: 0/1
  --video VIDEO, -v VIDEO
                        video feed path
  --data DATA, -d DATA  data definition file
  --config CONFIG, -c CONFIG
                        network configuration file
  --weights WEIGHTS [WEIGHTS ...], -w WEIGHTS [WEIGHTS ...]
                        initial weights file
  --roi_weights ROI_WEIGHTS [ROI_WEIGHTS ...], -wr ROI_WEIGHTS [ROI_WEIGHTS ...]
                        initial roi weights file
  --bgs BGS, -b BGS     Usage of BGS Motion Detection 0/1: No/Yes
  --conf_thresh CONF_THRESH, -ct CONF_THRESH
                        Confidence threshold at which detector will be
                        inferred
  --use_class USE_CLASS, -uc USE_CLASS
                        Flag to use roiclass 0/1:No/Yes
* $python test_detector_roiclass.py -v <path-to-video-frame-folder>` -vf <0/1: Video/Frames> -d <path-to-datafile> -w <path-to-weightfile> -wr <path-to-roiclass-weightfile> -b <bgs:ON/OFF:::0/1> -ct <confidence-threshold value> -uc <Flag to use roiclass::: 0/1: Yes/No>

```
Some important points to remember while running the inference script

1. `test_detector_roiclass.py` contains `USE_CLASS` flag. This flag indicates whether to use the classifier branch or not. If it is set to `True`, then all the detector results
	will be passed to classifier branch for further refinements and vice versa.

2. While mentioning the weights for the classifier branch in -wr switch of inference_video.py code, don't write the whole name of the weight file. Only write till `*_roi_` as I have mentioned below in the given demo command. 
	It automatically takes the name from the configuration file of yolov3_person_roi.cfg from the field name class_size of roiclass layer.

3. `video_feed` flag used in the command line argument indicates whether to use video or frame sequences as the input to teh detector. Setting it to 0 or 1 will take video and frame sequence 
	input respectively. 
	
4. All the detection results and texts files are stored in `detection_results` and `detection_texts` folders respectively. Also, there is a `DEBUG` flag that is passed to the inference class wrapper 
	`YOLOv3ROIClassTester`. All the detection output images will be stored only if this flag is `True`
5. There is a layer named roiclass in the CFG FIles of YOLOv3. You can check that configuration file in the folder INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/yolo3_person_roi.cfg.
	You have to add this layer in the configuration to support the classifier branch. By default, it is already added.
	
Demo Command:

$`python test_detector_roiclass.py -v INGRAM_DATA/ -vf 1 -d cfg/coco.data -c cfg/yolov3_person_roi.cfg -w INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/yolov3_person_final.weights -wr INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/000356_roi_`

___

### How to run the training script ###

Python file: `train_detector_roiclass.py`

Usage: 


```
$ python train_detector_roiclass.py --help

train_detector_roiclass.py [-h] [--data DATA] [--config CONFIG]
                                  [--weights WEIGHTS]
                                  [--weights_roi WEIGHTS_ROI] [--reset]

optional arguments:
  -h, --help            show this help message and exit
  --data DATA, -d DATA  data definition file
  --config CONFIG, -c CONFIG
                        network configuration file
  --weights WEIGHTS, -w WEIGHTS
                        initial weights file
  --weights_roi WEIGHTS_ROI, -wr WEIGHTS_ROI
                        initial weights file roi
  --reset, -r           initialize the epoch and model seen value


* $python train_detector.py -d <path-to-data-file> -c <path-to-cfg-file> -w <path-to-weightfile> -wr <path-to-roiclass-weightfile> -r <reset flag>

```

Some import points to remember while running the training script

- `trainer.py` contains the YOLOv3ROIClassTrainer takes optional argument, FREEZE_DETECTOR flag. If this flag is on, whole detector network will be freezed and only branch training will be taken
	into consideration. Also, as the classifier contains Fully Connected Layer, it is important to train on the fixed resolution. Resolution will be taken from the CFG file of the detector.
- FREEZE_DETECTOR Flag is on by default. Freezing the detector is recommended as the training will be very fast and efficient only on the branch gradient as seen from the experiments. 
- To run the branch classifier training, it is mandatory to mention `[roiclass]` switch in configuration file of the detector. 
   It takes, `in_filters`, `out_filters`, `class_size`, `scaled_window` and `spatial_scale` as parameters. You can attach the branch at any stride level. ** Stride = 32 ** is recommended for getting
   FPN architecture into working.
   
```

[roiclass]
scaled_window = 7 # This is the paramter of ROIPooling in ROIClass. 

in_filters = 64 # This is the number of input filters in ROIClass. Generally, this parameter is same as the output of the previous layer. 

out_filters = 64 # This is the number of output filters in ROIClass Convolutional Layers.

class_size = small # This is the size constraint of the ROIClass. This parameter controls the size of the proposals to train on. Small, Medium, and Large are based on COCO Standards.
					small = AREA < 32 * 32, medium = 32 * 32 <= Area < 96 * 96, large = Area >= 96 * 96.
					NOTE: By default, it is set to `all` in CFG File. This option will take all the three scales into consideration during the training.
spatial_scale = 0.125 # Scale of feature map wrt original image size. For example, considering 608x608 image size, 76X76 feature map would have a spatial scale of 76 / 608 = 0.125
```
- All the other configurations of the branch optimizers and learning rate are same as the detector's connfiguration file mentions.


Demo Command: 

`$python train_detector_roiclass.py -c INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/yolov3_person_roi.cfg -d cfg/ingram.data -w INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/yolov3_person_final.weights -wr INGRAM_MODELS_WITH_ROICLASS/yolov3_person_pruned_roiclass/000356_roi_ -r 
`
___

*** New FPN-alike structure for ROIClass ***

![FPN Flow for ROIClass](data/proposed_architecture_2.png)

*** TIME BENCHMARKS ***

NOTE: Time Benchmarking is done from the STEM of YOLO Model to the Classifier Branch.

Specification: `Intel® Core™ i5-7400 CPU @ 3.00GHz × 4`

Without `ROIFPN: 0.142` seconds 
With `ROIFPN: 0.176` seconds

___


### Integration of YOLOv3 + ROICLASS detector with the present Motion Detection/Background Subtraction Pipeline ###

There is a working code available of the motion detection using BGS algorithms and shallow neural networks. Link of the repository: [Github](https://uncannyvision.visualstudio.com/bitbucket-PerformanceOptimisation/_git/bgs_motion_detection.git)

Presently, this repository is having `GSOC` and `LSBP` algorithms support. These are the contributor algorithms that are not available in legacy version of `opencv` repo. You have to
compile and install the python executables of these algorithm from `opencv_contrib` by using `-DOPENCV_EXTRA_MODULES` switch. Detailed installation instructions can be found in README file.

This repo is built to work better on the sequential frame input such as video as it contains `update_bg` to change the background model statistics of BGS algorithm. If you want to infer on the 
frame input(not necessary sequential), you can change the BGS algorithm to any other simple baselines such as MOG, MOG2, KNN, KDE, etc and can comment out `update_bg` function in the file `bgs_opencv.py`


### UPDATE ###

This repository now contains the support of integration of BGS Motion Detection Pipeline. NOTE: BGS Pipeline is stable only on video streams.

To enable BGS support, keep `INTEGRATE_BGS=True` in `test_detector_roiclass.py` script.

`bgsrunner.py` contains the class definition of BGS Motion Detection Piepeline support with YOLOv3 SST. Class is being initialized in `yolov3_roiclass.py` aliased as `runner` module


`Cyan` and `Orange` boxes are the one detected by the BGS pipeline. The former means foreground and the latter signifies the background.

### NOTE ###

Before using the OpenCV's library of GSOC/LSBP algorithms, please visit [this](https://uncannyvision.visualstudio.com/bitbucket-PerformanceOptimisation/_git/bgs_motion_detection.git) link for proper 
installation of the modified scripts.

All the INGRAM related model and ROICLASS weights files can be accessed from [this](https://drive.google.com/drive/folders/1Go6OefROy7r4uCB_Jku3XO_n8FILGHln?usp=sharing) link

___

