'''
Author: Shreejal Trivedi

Date: 16th July 2020

Description: This is the inference testing code of Detector + Classifier Pipeline

Command Line Usage: python test_detector_roiclass.py -v <path-to-input-video/frames> -vf <video-feed: 0/1--> Video/Frame> 
                    -d <path-to-datafile> -c <path-to-cfgfile> -w <path-to-weightfile> -wr <path-to-roiclass-weightfile> -b <bgs_flag: --> ON/OFF>

'''
from tester import YOLOv3ROIClassTester
import cv2
import os
from tqdm import tqdm
import argparse
import zmq
import json
import random
import time

# TEST ON FRAMES

def run_pipeline_frames(VIDEO_DEED, detector):

    print("Frame Sequence Input!!")

    '''
    setup zmq
    '''
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.bind("tcp://*:%s" % str(port))

    print('outside')
    start = False
    while start==False:
        command = str(socket.recv().decode('utf-8'))
        if command=='start container':
            start = True

    print('outside')

    frames = list(map(lambda x: os.path.join(VIDEO_FEED, x), os.listdir(VIDEO_FEED)))
    frames.sort()
    print(frames)
    print(VIDEO_FEED)
    for frame in tqdm(frames):
        print(frame)
        data = cv2.imread(frame)
        data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
        results = detector.run_detector(data, os.path.basename(frame))

        topic = random.randrange(9999,10005)
        messagedata = random.randrange(1,215) - 80
        socket.send_json( results)

    completion = {'value':'completion'}
    socket.send_json(completion)


# TEST ON VIDEO
def run_pipeline_video(VIDEO_FEED, detector):

    print("Video Input!!")
    count = 0
    capture = cv2.VideoCapture(VIDEO_FEED)
    while not capture.isOpened():
        capture = cv2.VideoCapture(VIDEO_FEED)
        cv2.waitKey(1000)
        print("Wait for the header")
    _ = capture.get(1)

    while True:
        flag, frame = capture.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if flag:  
            _ = capture.get(1)
            detector.run_detector(frame, 'frame' + str(count).zfill(6) + '.jpg')
            print("Frame: %s"%(str(count).zfill(6) + '.jpg'))
            count +=1
        else:
            cv2.waitKey(1000)
            break
        if 0xFF & cv2.waitKey(10) == 27:
            break
    cv2.destroyAllWindows()


# Testbench == SUCCESS
if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--video_feed', '-vf', type=int, 
        default=0, help='Video/Frame: 0/1')
    parser.add_argument('--video', '-v', type=str, 
        default='cfg/sketch.mp4', help='video feed path')
    parser.add_argument('--data', '-d',    type=str, 
        default='cfg/sketch.data', help='data definition file')
    parser.add_argument('--config', '-c',  type=str, 
        default='cfg/sketch.cfg', help='network configuration file')
    parser.add_argument('--weights', '-w', type=str, nargs='+', 
        default=['weights/yolov3.weights'], help='initial weights file')
    parser.add_argument('--roi_weights', '-wr', type=str, nargs='+', 
        default=['weights/yolov3_roi.weights'], help='initial roi weights file')
    parser.add_argument('--bgs', '-b', type=int, 
        default=0, help='Usage of BGS Motion Detection 0/1: No/Yes')
    parser.add_argument('--conf_thresh', '-ct', type=float,
        default=0.01, help='Confidence threshold at which detector will be inferred')
    parser.add_argument('--use_class', '-uc', type=int, 
        default=1, help='Flag to use roiclass 0/1:No/Yes')
    
    parser.add_argument('--port', '-p', type=int,
        default=1, help='Flag for port')


    FLAGS, _ = parser.parse_known_args()
    VIDEO_FEED = FLAGS.video
    port = FLAGS.port


    if FLAGS.bgs:
        INTEGRATE_BGS = True
    else:
        INTEGRATE_BGS = False
    
    if FLAGS.use_class:
        USE_CLASS = True
    else:
        USE_CLASS = False

    detector = YOLOv3ROIClassTester(FLAGS.data, FLAGS.config, FLAGS.weights[0], FLAGS.roi_weights[0], conf_thresh=FLAGS.conf_thresh, USE_CLASS=USE_CLASS, DEBUG=True, INTEGRATE_BGS=INTEGRATE_BGS, max_prop_thresh=1)
    # print(detector.model)

    if FLAGS.video_feed:
        run_pipeline_frames(VIDEO_FEED, detector)
    else:
        run_pipeline_video(VIDEO_FEED, detector)
